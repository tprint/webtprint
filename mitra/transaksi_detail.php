<?php 
    $page='transaksi';
    include '../admin/core/init.php';
    include 'include/header.php';
    include 'include/navbar.php';
    
    // Session    
	session_start();
    if(!isset($_SESSION['login_mitra'])) {
      header('location:login/index.php');
    }else {
      $login_mitra = $_SESSION['login_mitra'];
    }

    $id_transaksi=$_GET['id'];
    $icon_loct = "http://icon.tprint.web.id/";
    $query="SELECT tbl_transaksi.tgl_pesan,
                tbl_transaksi.id_transaksi,
                tbl_transaksi.tgl_delivery,
                tbl_transaksi.tgl_selesai,
                tbl_transaksi.status,
                tbl_transaksi.status,
                tbl_transaksi.copy_qty,
                tbl_transaksi.paper_qty,
                tbl_transaksi.keterangan,
                tbl_transaksi.id_maps,
                tbl_user.nama AS nama_user,
                tbl_user.telp AS telepon_user,
                tbl_file.nama AS nama_file,
                tbl_kategori.nama AS nama_kategori,
                tbl_kategori.icon AS icon_kategori,
                tbl_produk.ukuran AS ukuran_produk,
                tbl_transaksi.harga_satuan,
                tbl_transaksi.harga_total,
                tbl_transaksi.ongkir
            FROM tbl_transaksi
            JOIN tbl_user ON tbl_transaksi.id_user=tbl_user.id_user
            JOIN tbl_produk ON tbl_transaksi.id_produk=tbl_produk.id_produk AND tbl_transaksi.id_mitra=tbl_produk.id_mitra
            JOIN tbl_kategori ON tbl_produk.id_kategori=tbl_kategori.id_kategori
            JOIN tbl_file ON tbl_transaksi.id_file=tbl_file.id_file
            WHERE tbl_transaksi.id_mitra='$login_mitra' AND tbl_transaksi.id_transaksi='$id_transaksi'";
    $result = mysqli_query($conn, $query);
    $row=$result->fetch_assoc();
    $status=$row['status'];
?>
<br><br><br><br><br>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="">
                <div class="card-header">
                    <h4 class="card-title"> Detail Order </h4>
                    <div align="right">
                        <?php if($status == 0) { ?>
                            <a href="transaksi.php?status=1&&kirim=<?php echo $id_transaksi;?>" class="btn-primary btn-sm">Kirim</a>
                        <?php }else if($status == 1) { ?>
                            <a href="transaksi.php?status=2&&done=<?php echo $id_transaksi;?>" class="btn-success btn-sm">Selesai</a>
                        <?php }?>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="row">
                                <div class="card-icon col-2 col-md-2 col-xs-4">
                                    
                                </div>
                                <div class="col-4 col-md-6 col-xs-4">
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            
                        </div>
                    </div>
                </div>	
                <div class="main-panel">
                    <div class="content">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <tbody>
                                                        <tr>
                                                        <?php
                                                            if($status==0){
                                                                echo "<td>Tanggal Pesan    : ",$row['tgl_pesan'], "</td>";
                                                            }
                                                            if($status==1){
                                                                echo "<td>Tanggal Pengiriman    : ",$row['tgl_delivery'], "</td>";
                                                            }
                                                            if($status==2){
                                                                echo "<td>Tanggal Selesai    : ",$row['tgl_selesai'], "</td>";
                                                            }
                                                        ?>
                                                        </tr>
                                                        <tr>
                                                            <td>Nama    : <?php echo $row['nama_user']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>No. Telepon  : <?php echo $row['telepon_user'];?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Keterangan : <?php echo $row['keterangan'];?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Alamat Pengiriman : <?php echo $row['id_maps'];?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <tbody>
                                                        
                                                        <tr>
                                                            <td>
                                                                <table>
                                                                    <tbody> 
                                                                        <tr>
                                                                            <td>
                                                                            <img src="<?php echo $icon_loct, $row['icon_kategori']; ?>" width="50" height="50">
                                                                            </td>
                                                                            <td>
                                                                            <?php $kalimat = $row['nama_file']; $sub_kalimat = substr($kalimat, 16);?> <b><a href="../admin/api/upload/file/<?php echo $sub_kalimat;?>"><?php echo $kalimat;?></a></b>
                                                                            <br> <?php echo $row['paper_qty']; ?> Halaman, <?php echo $row['ukuran_produk']; ?>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>                                                            
                                                        </tr>
                                                        <tr>
                                                            <td>Jumlah Cetak : <?php echo $row['copy_qty'];?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Harga Satuan : Rp <?php echo number_format($row['harga_satuan'],0,".","."); ?>  </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Harga Total : Rp <?php echo number_format($row['harga_total'],0,".","."); ?>  </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Ongkos Kirim : Rp <?php echo number_format($row['ongkir'],0,".","."); ?>  </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>							
            </div>
        </div>
    </div>
</div>  





<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/stellar.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>