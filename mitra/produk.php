<?php
    $page = 'produk';
    include '../admin/core/init.php';
	include 'include/header.php';
	include 'include/navbar.php';

    session_start();
    if(!isset($_SESSION['login_mitra'])) {
        header('location:login/index.php');
    }else {
    $login_mitra = $_SESSION['login_mitra'];
    }
    

    //Query select produk by mitra
    $query="SELECT * FROM 
                     tbl_produk, tbl_kategori 
                     WHERE tbl_produk.id_mitra='".$login_mitra."'
                     AND tbl_produk.id_kategori=tbl_kategori.id_kategori";
    $result = mysqli_query($conn, $query);

?>

<!-- Content -->
<body>

<br>
<br>
<br>
<br>
<br>
    <div id="home">
        <div class="container"> 
            <div class="row">
              <div class="card-body">
                <a href="addproduk.php"><button type="submit" class="btn btn-primary btn-sm pull-right">Add Produk</button></a>
                <br/><br/>
                <div class="table">
                    <table class="table table-striped table-hover">
                        <thead class=" text-primary lead">
                            <tr>
                            <th>ID Produk</th>
                            <th>Kategori</th>
                            <th>Ukuran</th>
                            <th>Bahan</th>
                            <th>Harga berwarna</th>
                            <th>Harga hitam</th>
                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if ($result) {
                                while($row=$result->fetch_assoc()) { ?>
                                <tr >
                                    <td><?php echo $row['id_produk']; ?></td>
                                    <td><?php echo $row['nama']; ?></td>
                                    <td><?php echo $row['ukuran']; ?></td>
                                    <td><?php echo $row['bahan']; ?></td>
                                    <td>Rp <?php echo number_format($row['price_c'],0,".","."); ?></td>
                                    <td>Rp <?php echo number_format($row['price_b'],0,".","."); ?></td>
                                    <td>
                                        <a href="editproduk.php?id=<?php echo $row['id_produk']?>">
                                            <button type="submit" class="btn btn-primary btn-sm pull-right">Edit</button>
                                        </a>
                                    </td>
                                </tr>
                            <?php 
                                }
                            }else{
                                echo "<tr><td colspan='7'>Belum ada produk.</td></tr>";
                            } ?>
                        </tbody>
                    </table>           
                </div>
            </div>
        </div>
    </div>
</div>          
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/stellar.js"></script>
</body>
</html>