<?php 
    include '../../admin/api/config/database.php';
	include '../../admin/api/objects/id_generator.php';
	include '../../admin/core/init.php';
	
	// instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();
    
    // initialize object
	$id_generator = new IDGenerator($db);
	
	session_start();
	if(isset($_SESSION['login_mitra'])) {
		header('location:../index.php');
	}
	if (isset($_POST["login"])) {		
		$id = $id_generator->mitra();
		$nama = $_POST["nama"];
		$telp = $_POST["telepon"];
		$provinsi = $_POST["provinsi_text"];
		$kota = $_POST["kota_text"];
		$kecamatan = $_POST["kecamatan_text"];
		$alamat = $_POST["alamat"];
		$kodepos = $_POST["kodepos"];
		$email = $_POST["email"];
		$password = md5($_POST["password"]);
		$Repassword = md5($_POST["confirmpassword"]);
		if($password==$Repassword){
			$query = "INSERT INTO tbl_mitra (id_mitra, nama, telp, email, password) VALUES ('$id', '$nama', '$telp','$email','$password')";
			$result = mysqli_query($conn, $query);
			
			//$row = mysqli_fetch_array($result);
			if ($result) {
				
				$insert_alamat = mysqli_query($conn, "INSERT INTO tbl_alamat (id_mitra, alamat, provinsi, kota, kecamatan, kode_post) VALUES ('$id', '$alamat', '$provinsi', '$kota', '$kecamatan', '$kodepos')");
				
				header('location:index.php');
			}else{
				echo "<p>Error: " . $query . "<br>" . mysqli_error($conn). "</p>";
			}
		}else{
			echo "<p>Password not match </p>";
		}
		//$query = "SELECT * FROM tbl_mitra WHERE email='$email' AND password='".md5($password)."' LIMIT 0,1";
		
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Register</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	 <link rel="icon" type="img/png" href="../img/Logo.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<Style>
		/* Hide HTML5 Up and Down arrows. */
		input[type="number"]::-webkit-outer-spin-button, input[type="number"]::-webkit-inner-spin-button {
			-webkit-appearance: none;
			margin: 0;
		}
		
		input[type="number"] {
			-moz-appearance: textfield;
		}
	</Style>
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-b-160 p-t-50">
				<form class="login100-form validate-form" id="contactForm" method="POST">
					
					<span class="login100-form-title p-b-43">
						Register Website Mitra
					</span>
					
					<div class="wrap-input100 rs1 rs2 validate-input" style="width: 100%;" data-validate = "Email required">
						<input class="input100" id="email" type="text" name="nama">
						<span class="label-input100">Nama</span>
					</div>	

					<div class="wrap-input100 rs3 validate-input" style="width: 100%;" data-validate = "Email required">
						<input class="input100" id="telpon" type="number" name="telepon">
						<span class="label-input100">No Telepon</span>
					</div>	

					<div class="wrap-input100 rs3 validate-input" style="width: 100%;" data-validate = "Email required">
							<div class="col-sm-12">
                                <div class="row">                                    
                                    <div class="form-group col-sm-4">
                                        <select name="provinsi" class="form-control" id="provinsi" style="border: none;" >
                                            <option value="">Provinsi</option>
										</select>
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <select name="kota" class="form-control" id="kota" style="border: none;">
                                            <option value="">Kota</option>
                                        </select>
                                    </div>
                                    
                                    <div class="form-group col-sm-4">
                                        <select name="kecamatan" class="form-control" id="kecamatan" style="border: none;">
                                            <option value="">Kecamatan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
					</div>	

					<div class="wrap-input100 rs3 validate-input" style="width: 80%;" data-validate = "Email required">
						<input class="input100" id="alamat" type="text" name="alamat">
						<span class="label-input100">Alamat</span>
					</div>	

					<div class="wrap-input100 rs3 validate-input" style="width: 20%;" data-validate = "Email required">
						<input class="input100" id="kodepos" type="number" name="kodepos">
						<span class="label-input100">Kode Pos</span>
					</div>

					<div class="wrap-input100 rs3 validate-input" style="width: 100%;" data-validate = "Email required">
						<input class="input100" id="email" type="email" name="email">
						<span class="label-input100">Email</span>
					</div>					
					
					<div class="wrap-input100 rs3 validate-input" data-validate="Password is required">
						<input class="input100" id="password" type="password" name="password">
						<span class="label-input100">Password</span>
					</div>

					<div class="wrap-input100 rs4 validate-input" data-validate="">
						<input class="input100" id="confirmpassword" type="password" name="confirmpassword">
						<span class="label-input100">Confirm Password</span>
					</div>
						<input type="text" name="provinsi_text" style="display:none" id="hiddenValprov" />
						<input type="text" name="kota_text" style="display:none" id="hiddenValkota" />
						<input type="text" name="kecamatan_text" style="display:none" id="hiddenValkec" />
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" name ="login" type="submit">
							Register
						</button>
					</div>
					</form>
					<div class="text-center w-full p-t-23">

						<a href="index.php" class="txt1">
							Login
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	

	
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript">
		$(document).ready(function() {
            $.ajax({
				url: '../provinsi.php',
				success: function(response) {
					$('#provinsi').html(response);
				}
			}); 
        });
        $(document).ready(function() {
            $("#provinsi").change(function() {
                var provinsi_id = $(this).val();                 
                $.ajax({
                    type:'POST',
                    url: '../kota.php',
                    data: 'prov_id='+provinsi_id,
                    success: function(response) {
                        $('#kota').html(response);
                    }
                });            
            })
        });
        $(document).ready(function() {
            $('#kota').change(function() {
                var kabupaten_id = $(this).val();

                $.ajax({
                    type:'POST',
                    url: '../kecamatan.php',
                    data: 'kab_id='+kabupaten_id,
                    success: function(response) {
                        $('#kecamatan').html(response);
                    }
                });            
            })
        });
    </script>
    <script type="text/javascript">
		$(document).ready(function(){
            $('#provinsi').change(function(){
                var provinsi = $("#provinsi option:selected").text();
                // alert("Selected Option Text: "+provinsi);
                document.getElementById("hiddenValprov").value = provinsi; 
            });

        });
        $(document).ready(function(){
            $('#kota').change(function(){
                var kota = $("#kota option:selected").text();
                //alert("Selected Option Text: "+kota);
                document.getElementById("hiddenValkota").value = kota;
            });

        });
        $(document).ready(function(){
            $('#kecamatan').change(function(){
                var kecamatan = $("#kecamatan option:selected").text();
                //alert("Selected Option Text: "+kecamatan);
                document.getElementById("hiddenValkec").value = kecamatan;
            });

        });
    </script> 
	<script>
		jQuery(document).ready( function($) {
		
			// Disable scroll when focused on a number input.
			$('form').on('focus', 'input[type=number]', function(e) {
				$(this).on('wheel', function(e) {
					e.preventDefault();
				});
			});
		
			// Restore scroll on number inputs.
			$('form').on('blur', 'input[type=number]', function(e) {
				$(this).off('wheel');
			});
		
			// Disable up and down keys.
			$('form').on('keydown', 'input[type=number]', function(e) {
				if ( e.which == 38 || e.which == 40 )
					e.preventDefault();
			});  
			
		});
	</script>

</body>
</html>