	<header class="header_area">
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<a class="navbar-brand logo_h" href="index.php"><img src="img/logo3.png" alt=""></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
					 aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
						<ul class="nav navbar-nav menu_nav justify-content-end">
							<li <?php if($page == "index") echo "class='nav-item active'"; else echo "class='nav-item'";?>>
								<a class="nav-link" href="index.php">Home</a>
							</li>
							<li <?php if($page == "transaksi") echo "class='nav-item active'"; else echo "class='nav-item'";?>>
								<a class="nav-link" href="transaksi.php?status=0">Transaksi</a>
							</li>
							<li <?php if($page == "produk") echo "class='nav-item active'"; else echo "class='nav-item'";?>>
								<a class="nav-link" href="produk.php">Produk</a>
							</li>
							<li <?php if($page == "profile") echo "class='nav-item active'"; else echo "class='nav-item'";?>>
								<a class="nav-link" href="profile.php">Profile</a>
							</li>
							<!--
							<li <?php if($page == "contact") echo "class='nav-item active'"; else echo "class='nav-item'";?>>
								<a class="nav-link" href="contact.php">Contact</a>
							</li>
							-->
							<li class='nav-item'>
								<a class="nav-link" href="logout.php">logout</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
	</header>