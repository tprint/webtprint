<?php
//$key is our base64 encoded 256bit key that we created earlier. You will probably store and define this key in a config file.
$key = 'VGVrYXNoaSBjaGkgbmktc2Fu';

function my_encrypt($data, $key) {
    // Remove the base64 encoding from our key
    $encryption_key = base64_decode($key);
    // Generate an initialization vector
    $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
    // Encrypt the data using AES 256 encryption in CBC mode using our encryption key and initialization vector.
    $encrypted = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $iv);
    // The $iv is just as important as the key for decrypting, so save it with our encrypted data using a unique separator (::)
    return base64_encode($encrypted . '::' . $iv);
}

function my_decrypt($data, $key) {
    // Remove the base64 encoding from our key
    $encryption_key = base64_decode($key);
    // To decrypt, split the encrypted data from our IV - our unique separator used was "::"
    list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);
    return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
}
?>
<?php
    $page='transaksi';
    include '../admin/core/init.php';
    include 'include/header.php';
    include 'include/navbar.php';
    
    // Session    
	session_start();
    if(!isset($_SESSION['login_mitra'])) {
      header('location:login/index.php');
    }else {
      $login_mitra = $_SESSION['login_mitra'];
    }

    //Query select transaksi 
    $status=$_GET['status'];
    $icon_loct = "http://icon.tprint.web.id/";
    $file_loct = "http://file.tprint.web.id/";
    $query="SELECT tbl_transaksi.tgl_pesan,
                tbl_transaksi.id_transaksi,
                tbl_transaksi.tgl_delivery,
                tbl_transaksi.tgl_selesai,
                tbl_transaksi.status,
                tbl_kategori.icon AS icon_kategori,
                tbl_user.nama AS nama_user,
                tbl_file.id_file AS id_file,
                tbl_file.nama AS nama_file,
                tbl_file.id_user AS folder_name,
                tbl_kategori.nama AS nama_kategori,
                tbl_produk.ukuran AS ukuran_produk,
                tbl_produk.bahan AS bahan_produk,
                tbl_transaksi.copy_qty AS salin,
                tbl_transaksi.harga_total AS harga,
                tbl_transaksi.ongkir
            FROM tbl_transaksi
            JOIN tbl_folder ON tbl_transaksi.id_user=tbl_folder.id_user
            JOIN tbl_user ON tbl_transaksi.id_user=tbl_user.id_user
            JOIN tbl_produk ON tbl_transaksi.id_produk=tbl_produk.id_produk
            JOIN tbl_kategori ON tbl_produk.id_kategori=tbl_kategori.id_kategori
            JOIN tbl_file ON tbl_transaksi.id_file=tbl_file.id_file
            WHERE tbl_transaksi.id_mitra='$login_mitra' AND tbl_transaksi.status='$status'";
    $result = mysqli_query($conn, $query);
    
    date_default_timezone_set("Asia/Jakarta");
    
    //Query Kirim transaksi
    if(isset($_GET['kirim']) && !empty($_GET['kirim'])){
        $id_transaksi = $_GET['kirim'];
        $query_kirim = "UPDATE tbl_transaksi 
                            SET status = 1,
                                tgl_delivery = date('Y-m-d H:i:s')
                            WHERE id_transaksi = '$id_transaksi'";
        mysqli_query($conn, $query_kirim);
        header('location:transaksi.php?status=1');
    }

    //Query done transaksi
    if(isset($_GET['done']) && !empty($_GET['done'])){
        $id_transaksi = $_GET['done'];
        $query_done = "UPDATE tbl_transaksi 
                        SET status=2,
                            tgl_selesai = date('Y-m-d H:i:s') 
                        WHERE id_transaksi = '$id_transaksi'";
        mysqli_query($conn, $query_done);
        header('location:transaksi.php?status=2');
    }
?>

<!--Content-->
<body>
<br><br><br><br><br>

<div class="main-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-8">
                <div class="responsive text-right hidden-sm hidden-xs">
                    <ul class="nav nav-pills">
                        <li class="'nav-item">
                            <a class="nav-link <?php if($status == 0) echo 'active';?>" href="transaksi.php?status=0">Belum Diproses</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php if($status == 1) echo 'active';?>" href="transaksi.php?status=1">Sedang Dikirim</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php if($status == 2) echo 'active';?>" href="transaksi.php?status=2">Selesai</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <br/>
        <div id="row"> 
            <div class="col-md-12 table-responsive">           
                <table class="table table-striped table-hover table table-bordered" id="belumproses">
                    <thead class="text-primary lead">
                        <tr>
                            <th>Tanggal</th>
                            <th>User</th>
                            <th>File</th>
                            <th>Ukuran</th>
                            <th>Harga</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if ($result) { 
                            while($row=$result->fetch_assoc()){
                                $url_file = $file_loct . "index.php?id=" . my_encrypt($row['folder_name'], $key) . "&&file=" . my_encrypt($row['nama_file'], $key);
                         ?>
                            <tr >
                                <?php
                                    if($status==0){
                                        echo "<td>";
                                        echo $row['tgl_pesan'];
                                        echo "</td>";
                                    }
                                    if($status==1){
                                        echo "<td>";
                                        echo $row['tgl_delivery'];
                                        echo "</td>";
                                    }
                                    if($status==2){
                                        echo "<td>";
                                        echo $row['tgl_selesai'];
                                        echo "</td>";
                                    }
                                ?>
                                <td><?php echo $row['nama_user']; ?></td>
                                <td>
                                    <div class="row">
                                        <div class="col-3">
                                            <img src="<?php echo $icon_loct, $row['icon_kategori']; ?>" width="50" height="50">
                                        </div>
                                        <div class="col-8">
                                            <?php echo $row['nama_kategori']; ?>
                                            <br>  <b><a href="<?php echo $url_file; ?>"><?php echo $row['nama_file'];?></a></b>
                                            
                                        </div>
                                    </div>
                                </td>
                                <td><?php echo $row['ukuran_produk']; ?></td>
                                <td>Rp <?php echo number_format($row['harga'],0,".","."); ?></td>
                                <td>   
                                    <?php if($status == 0) { ?>
                                        <a href="transaksi_detail.php?id=<?php echo $row['id_transaksi'];?>" class="btn-info btn-sm">Detail</a>
                                        <a href="transaksi.php?status=1&&kirim=<?php echo $row['id_transaksi'];?>" class="btn-primary btn-sm">Kirim</a>
                                    <?php }else if($status == 1) { ?>
                                        <a href="transaksi_detail.php?id=<?php echo $row['id_transaksi'];?>" class="btn-info btn-sm">Detail</a>
                                        <a href="transaksi.php?status=2&&done=<?php echo $row['id_transaksi'];?>" class="btn-success btn-sm">Selesai</a>
                                    <?php }else if($status == 2) {?>
                                        Selesai
                                    <?php }else{?>
                                        Cancel
                                    <?php }?>
                                </td>
                            </tr>
                        <?php 
                            }
                        }else{
                            echo "<tr><td colspan='7'>Belum ada transaksi.</td></tr>";
                        } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/stellar.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>