<?php
session_start();
$page = 'profile';
// include 'provinsi.php';
include 'include/header.php';
include 'include/navbar.php';
include '../admin/core/init.php';


//$result = $conn->query("SELECT * FROM tbl_mitra,tbl_alamat WHERE tbl_mitra.id_mitra='$_GET[id]' AND tbl_alamat.id_alamat=tbl_mitra.id_alamat ");
//$row =  mysqli_fetch_assoc($result);

//QUERY SELECT TO EDIT ALAMAT
if(isset($_GET['id']) && !empty($_GET['id'])){
    $id_mitra=$_GET['id'];
    $query = ("SELECT * FROM tbl_mitra,tbl_alamat 
                WHERE tbl_mitra.id_mitra='$_GET[id]' 
                AND tbl_alamat.id_mitra='$_GET[id]' 
                LIMIT 0,1");
    $result = mysqli_query($conn, $query) or die($conn->error);

}
?>

<!doctype html>
<html lang="en">




<!--================ Start Header Area =================-->

<!--================ End Header Area =================-->

<body>  
    <div id="home">
        <div class="site-header">
            <div class="top-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="left-header lead">
                                <a href="welcome.php"><img src="images/logoo2.png" width="280px" height="50px"></a>                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-header">
                <div class="container">
                    <div class="row">
                        <div class="text-center">
                            <div class="logo">
                                <h1><a href="#" title="Dreri">Edit Profile</a></h1>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-6">
                            <div class="menu text-right hidden-sm hidden-xs">  
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div id="services"> 
        <div>
            <div class="title-section text-center"></div>
            <div class="container lead">
                <div class="col-md-12">
                    <form enctype="multipart/form-data" method="post">
                    <?php
                            while($pecah=$result->fetch_assoc()){
                        ?>
                        <div class="form-group row">
                            <label for="colFormLabel" class="col-sm-2 col-form-label">Nama Mitra</label>
                            <div class="col-sm-10">
                                <input type="text" name="nama" class="form-control" id="colFormLabel" value="<?php echo $pecah['nama']; ?>">
                            </div>
                        </div>
                        <label></label>
                        <div class="form-group row">
                            <div class="col-md-10">
                                <div class="row col-sm-12">
                                    <div class="form-group col-sm-4">
                                        <select name="provinsi" class="form-control" id="provinsi">
                                            <option value=""></option>
										</select>
                                    </div> 
                                    <div class="form-group col-sm-4">
                                        <select name="kota" class="form-control" id="kota">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    
                                    <div class="form-group col-sm-4">
                                        <select name="kecamatan" class="form-control" id="kecamatan">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                                            
                        <div class="form-group row">
                            <label for="colFormLabel" class="col-sm-2 col-form-label">Alamat Mitra</label>
                            <div class="col-sm-10">
                                <input type="text" name="alamat" class="form-control" id="colFormLabel" value="<?php echo $pecah['alamat']; ?>" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabel" class="col-sm-2 col-form-label">Telepon Kantor</label>
                            <div class="col-sm-10">
                                <input type="number" name="telp" class="form-control" id="colFormLabel" value="<?php echo $pecah['telp']; ?>" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabel" class="col-sm-2 col-form-label">Email Kantor</label>
                            <div class="col-sm-10">
                                <input type="text" name="email" class="form-control" id="colFormLabel" value="<?php echo $pecah['email']; ?>" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colFormLabel" class="col-sm-2 col-form-label">Foto Profil</label>
                            <div class="col-sm-10 form-group form-file-upload form-file-simple">
                                <input type="text" class="form-control inputFileVisible" value="<?php echo $pecah['logo'];?>" placeholder="Simple chooser...">
                                <input class="inputFileHidden" name="foto" type="file">
                            </div>
                        </div>
                        <input type="text" name="provinsi_text" style="display:none" id="hiddenValprov" />
						<input type="text" name="kota_text" style="display:none" id="hiddenValkota" />
						<input type="text" name="kecamatan_text" style="display:none" id="hiddenValkec" />
                        <?php
                            }
                        ?>
                        <p align="right">
                            <input class="btn btn-primary" type="submit" value="SAVE" name="save"/>
                            <a class="btn btn-danger" href="profile.php">Cancel</a>
                        </p>
                    </form>

                    <!--Query Edit Profile-->
                    <?php
                    if (isset($_POST['save'])){
                        $namafoto=explode(".",$_FILES['foto']['name']);
                        $lokasi=$_FILES['foto']['tmp_name'];
                        $nama=$_POST['nama'];
                        $alamat=$_POST['alamat'];
                        $email=$_POST['email'];
                        $telp=$_POST['telp'];
                        $provinsi=$_POST['provinsi_text'];
                        $kota=$_POST['kota_text'];
                        $kecamatan=$_POST['kecamatan_text'];
                        $newfoto = $id_mitra.".".$namafoto[1];
                    
                        if ($namafoto != ''){
                            move_uploaded_file($lokasi, "../admin/upload/mitra_profile/$newfoto");
                            
                            $query = "UPDATE tbl_mitra, tbl_alamat SET  tbl_mitra.nama='$nama', tbl_mitra.telp='$telp', tbl_mitra.email='$email', tbl_alamat.alamat='$alamat', tbl_mitra.email='$email', tbl_mitra.telp='$telp', tbl_alamat.provinsi='$provinsi', tbl_alamat.kota='$kota', tbl_alamat.kecamatan='$kecamatan', logo='$newfoto' WHERE tbl_alamat.id_mitra='$_GET[id]' AND tbl_mitra.id_mitra='$_GET[id]'";
                        } else {
                            $query = "UPDATE tbl_mitra, tbl_alamat SET  tbl_mitra.nama='$nama', tbl_mitra.telp='$telp', tbl_mitra.email='$email', tbl_alamat.alamat='$alamat', tbl_mitra.email='$email', tbl_mitra.telp='$telp', tbl_alamat.provinsi='$provinsi', tbl_alamat.kota='$kota', tbl_alamat.kecamatan='$kecamatan' WHERE tbl_alamat.id_mitra='$_GET[id]' AND tbl_mitra.id_mitra='$_GET[id]'";
                        }
                        $result = $conn->query($query);

                        if ($result) {
				
                            echo "<script>alert('Data was updated succesfully !');</script>";
                            echo "<script>location='profile.php';</script>";
                        }else{
                            echo "<p>Error: " . $query . "<br>" . mysqli_error($conn). "</p>";
                        }
                        

                    }else if (isset($_POST['cancel'])){
                        echo "<script>location='profile.php';</script>";
                    }
                    ?>
                </div>     
            </div>
        </div>
    </body>
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="login/vendor/jquery/jquery-3.2.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $.ajax({
				url: 'provinsi.php',
				success: function(response) {
					$('#provinsi').html(response);
				}
			}); 
        });
        $(document).ready(function() {
            $("#provinsi").change(function() {
                var provinsi_id = $(this).val();                 
                $.ajax({
                    type:'POST',
                    url: 'kota.php',
                    data: 'prov_id='+provinsi_id,
                    success: function(response) {
                        $('#kota').html(response);
                    }
                });            
            })
        });
        $(document).ready(function() {
            $('#kota').change(function() {
                var kabupaten_id = $(this).val();

                $.ajax({
                    type:'POST',
                    url: 'kecamatan.php',
                    data: 'kab_id='+kabupaten_id,
                    success: function(response) {
                        $('#kecamatan').html(response);
                    }
                });            
            })
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#provinsi').change(function(){
                var provinsi = $("#provinsi option:selected").text();
                //alert("Selected Option Text: "+provinsi);
                document.getElementById("hiddenValprov").value = provinsi; 
            });

        });
        $(document).ready(function(){
            $('#kota').change(function(){
                var kota = $("#kota option:selected").text();
                //alert("Selected Option Text: "+kota);
                document.getElementById("hiddenValkota").value = kota;
            });

        });
        $(document).ready(function(){
            $('#kecamatan').change(function(){
                var kecamatan = $("#kecamatan option:selected").text();
                //alert("Selected Option Text: "+kecamatan);
                document.getElementById("hiddenValkec").value = kecamatan;
            });

        });
    </script> 
    <?php include 'include/footer.php'; ?>
</html>