<?php
$page = "profile";
include '../admin/core/init.php';
include 'include/navbar.php';
include 'include/header.php';

$pict_loct = "http://avatar.tprint.web.id/";
session_start();
    if(!isset($_SESSION['login_mitra'])) {
      header('location:login/index.php');
    }else {
      $login_mitra = $_SESSION['login_mitra'];
    }
$result = $conn->query("SELECT * FROM tbl_mitra,tbl_alamat WHERE tbl_mitra.id_mitra='$login_mitra' AND tbl_alamat.id_mitra='$login_mitra'");

$row = $result->fetch_assoc();


?>
<br>
<br>
<br>
<br>
<br>
<br>
<br>


<div class="container" style="align-content: center;">
	
	<div class="row">
		<div class="panel panel-primary col-md-4">
			<div class="panel-body">
				<img src="<?php echo $pict_loct, $row['logo']; ?>" class="img-responsive" style="height:250px" >
			</div>
		</div>
		<div class="col-md-8">
			<div class="table">
				<table class="table table-striped table-hover">                            
					
					<tbody class="h5">
						<tr >
							<td>Nama Mitra</td>
							<td>: </td>
							<td><?php echo $row['nama']; ?></td>
							<td> </td>
						</tr>
						
						<tr>
							<td>Alamat Mitra</td>
							<td>: </td>
							<td><?php echo $row['alamat'],", ",$row['kecamatan'],", ",$row['kota'],", ",$row['provinsi']; ?></td>
							<td> </td>
						</tr>
						<tr>
							<td>Telepon Kantor</td>
							<td>: </td>
							<td><?php echo $row['telp']; ?></td>
							<td> </td>
						</tr>
						<tr>
							<td>Email Kantor</td>
							<td>: </td>
							<td><?php echo $row['email']; ?></td>
							<td> </td>
						</tr>
						
					</tbody>
				</table>
			</div>
			<a class="btn-primary btn-sm pull-right" href="edit_profil.php?id=<?php echo $row['id_mitra'];?>">Edit Profile</a><br><br>
		</div>

	</div>
</div>
</body>

</html>