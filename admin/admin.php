<?php
    $page = 'Admin';
    
    // session
    session_start();
    if(!isset($_SESSION['loginadmin'])) {
      header('location:login.php');
    }else {
      $loginadmin = $_SESSION['loginadmin'];
    }

	include 'core/init.php';
    include 'include/header.php';
    include 'include/sidebar.php';
    include 'include/navbar.php';

    
    $id = 0;
    $mode = "";
    if (isset($_GET['id'])) {
        $id = $_GET['id'];
    }
    if (isset($_GET['mode'])) {
        $mode = $_GET['mode'];
    }
?>

    <div class="main-panel">
        <!-- Content -->
        <div class="content">
            <div class="container-fluid">            
            <!-- Form Add Mode -->
            <?php
                if ($mode=="add") {
            ?>
                <div class="card">
                    <div class="card-header card-header-rose">
                        <h4 class="card-title">Add Admin</h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>Masukkan Nama</label>
                                <input class="form-control" placeholder="your name" type="text" name="nama" required>
                            </div>
                            <div class="form-group">
                                <label>Masukkan Email</label>
                                <input class="form-control" placeholder="your email" type="email" name="admin_email" required >
                            </div>
                            <div class="form-group">
                                <label>Masukkan Password</label>
                                <input class="form-control" placeholder="your password" type="password" name="password" id="pass" required><br>
                                <input type="checkbox" onclick="showPass()">Show Password
                            </div>
                                <input class="btn btn-rose pull-right" type="submit" name="add" value="Add">
                            
                            <?php
                            //QUERY INPUT DATA ADD ADMIN
                            if (isset($_POST['add'])){
                                $nama= $_POST['nama'];
                                $password= md5($_POST['password']);
                                $email= $_POST['admin_email'];

                                $conn->query("INSERT INTO tbl_admin VALUES ('','$nama', '$email', '$password')");
                                echo "<script>alert('Data sucessfully saved !');</script>";
                                echo "<script>location='admin.php';</script>";
                            }
                            ?>

                            <script>
                                function showPass(){
                                    var x = document.getElementById("pass");
                                    if (x.type === "password"){
                                        x.type = "text";
                                    }else{
                                        x.type = "password";
                                    }
                                }
                            </script>
                        </form>
                    </div><!--End Card Body-->
                </div><!-- End Card-->            
            <?php                
                }elseif ($mode=="edit") {
            ?>
            <!-- Form Edit Mode -->
            <div class="card">
                    <div class="card-header card-header-rose">
                        <h4 class="card-title">Edit Admin</h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" enctype="multipart/form-data">
                            <?php 
                                $result = $conn->query("SELECT * FROM tbl_admin WHERE id_admin='$id'");
                                while ($row = $result->fetch_assoc()) {
                            ?>
                            <div class="form-group">
                                <label>Masukkan Nama</label>
                                <input class="form-control" placeholder="your name" type="text" name="nama" value="<?php echo $row['nama'];?>" required>
                            </div>
                            <div class="form-group">
                                <label>Masukkan Email</label>
                                <input class="form-control" placeholder="your email" type="email" name="admin_email" value="<?php echo $row['email'];?>" required >
                            </div>
                            <input class="btn btn-rose pull-right" type="submit" name="edit" value="Edit">
                            <?php } 
                                //QUERY INPUT DATA ADD ADMIN
                                if (isset($_POST['edit'])){
                                    $nama= $_POST['nama'];
                                    $email= $_POST['admin_email'];

                                    $conn->query("UPDATE tbl_admin SET nama='$nama', email='$email' WHERE id_admin='$id'");
                                    echo "<script>alert('Data sucessfully saved !');</script>";
                                    echo "<script>location='admin.php';</script>";
                                }
                            ?>
                        </form>       
                    </div><!--End Card Body-->
                </div><!-- End Card-->
            <?php
                } elseif ($mode=="delete") {
                    $conn->query("DELETE FROM tbl_admin WHERE id_admin='$id'");
                    echo "<script>alert('Data sucessfully deleted !');</script>";
                    echo "<script>location='admin.php';</script>";
                }else{
                
            ?>
                <div class="card">
                    <div class="card-header card-header-tabs card-header-rose">
                        <h4 class="nav-tabs-title">Tabel Admin</h4>
                        <ul class="nav nav-tabs pull-right">
                            <li class="nav-item">
                                <a href="?mode=add" class="nav-link active" rel="tooltip" data-original-title="Add New Admin"><i class="material-icons">add_circle_outline</i> Add</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table cellspacing="0" class="table" id="dtBasicExample" width="100%">
                                <thead class="text-primary">
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $nomor=1; 
                                    $ambil=$conn->query("SELECT * FROM tbl_admin"); 
                                    while ($row=$ambil->fetch_assoc()){ 
                                    ?>
                                    <tr>
                                        <td><?php echo $nomor ?></td>
                                        <td><?php echo $row['nama']; ?></td>
                                        <td><?php echo $row['email']; ?></td>
                                        <td>
                                            <a href="?mode=edit&&id=<?php echo $row['id_admin'];?>" class="btn btn-info btn-sm" rel="tooltip" data-original-title="Edit Admin">Edit</a>
                                            <a href="?mode=delete&&id=<?php echo $row['id_admin'];?>" class="btn btn-danger btn-sm" rel="tooltip" data-original-title="Delete Admin">Delete</a>
                                        </td>
                                    </tr>
                                    <?php $nomor++;} ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--End Card-->
            <?php } ?>
            </div>
        </div>
	    <!-- End Content -->
	</div>
</body>
<?php
    include 'include/footer.php';
?>
</html>
