<?php
	$page = 'User';
	
	// session
	session_start();
	if(!isset($_SESSION['loginadmin'])) {
		header('location:login.php');
	}else {
		$loginadmin = $_SESSION['loginadmin'];
	}

	include 'core/init.php';
  include 'include/header.php';
	include 'include/sidebar.php';
  include 'include/navbar.php';

?>


		<div class="main-panel">
			<!-- Content -->
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header card-header-rose">
									<h4 class="card-title">Tabel User</h4>
								</div>
								<div class="card-body">
									<div class="table-responsive">
										<table cellspacing="0" class="table" id="dtBasicExample" width="100%">
											<thead class=" text-rose">
												<tr>
													<th>No.</th>
													<th>Nama</th>
													<th>Email</th>
													<th>No.Telp</th>
													<th>Register Date</th>
													<th>Last Seen</th>
												</tr>
											</thead>
											<tbody>
											<?php $nomor=1 ?>
											<?php $result=$conn->query("SELECT * FROM tbl_user")?>
											<?php while ($pecah = $result->fetch_assoc()) { ?>
												<tr>
													<td><?php echo $nomor; ?></td>
													<td><?php echo $pecah['nama']; ?></td>
													<td><?php echo $pecah['telp']; ?></td>
													<td><?php echo $pecah['email']; ?></td>
													<td><?php echo $pecah['register_date']; ?></td>
													<td><?php echo $pecah['last_login']; ?></td>
													<!--
													<td>
														<a href="user_detail.php?id=<?php echo $pecah['id_user']; ?>" class="btn btn-success btn-sm">Detail</a>
													</td>
												-->
												</tr>
												<?php $nomor++; ?>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				<!-- End Content -->
	</body>
<?php
    include 'include/footer.php';
?>
</html>
