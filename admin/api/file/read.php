<?php
    // required headers
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");

    // include database and object files
    include_once '../config/database.php';
    include_once '../objects/file.php';
    
    // instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();
    
    // initialize object
    $file = new File($db);
    
    if (isset($_POST['id_user'])) {
        $file->id_user = htmlspecialchars($_POST['id_user']);
    }

    //query file
    $stmt = $file->read();
    $num = $stmt->rowCount();

    //check if more than 0 record found
    if($num > 0){
        //file array
        $file_arr = array();
        $file_arr["file"] = array();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            // extract row
            // this will make $row['name'] to
            // just $name only
            extract($row);

            $file_item=array(
                "id_file" => $id_file,
                "id_user" => $id_user,
                "nama" => $nama,
                "halaman" => $halaman,
                "size" => $size
            );

            array_push($file_arr["file"], $file_item);
        }
    
        // set response code - 200 OK
        http_response_code(200);
    
        // show products data in json format
        echo json_encode($file_arr);
    }else{
 
        // set response code - 404 Not found
        http_response_code(404);
     
        // tell the user no products found
        echo json_encode(
            array("message" => "No file found.")
        );
    }
?>