<?php
    // required headers
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");

    // include database and object files
    include_once '../config/database.php';
    include_once '../objects/file.php';
    include_once '../objects/storage.php';

    // instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();

    // initialize object
    $file = new File($db);
    $storage = new Storage($db);
    $path = "../../upload/user_file";
    $respons = array();

    if(isset($_POST['id_user']) && isset($_POST['file_name'])){
        $id_user = $_POST['id_user'];
        $file_name = $_POST['file_name'];
        
        // get user data
        $user_storage = $storage->read($id_user)->fetch(PDO::FETCH_ASSOC); 
        $file_path = $path . '/' . $id_user . '/' . $file_name;

        // get file size for update folder
        $file_size = $file->getSize($id_user, $file_name);

        // initialize file object for detele
        $file->id_user = $id_user;
        $file->nama = $file_name;

        if($storage->update($id_user, ($user_storage['size'] - $file_size)) && $file->delete()){
            if (unlink($file_path)) {
                // set response code - 200 OK
                http_response_code(200);            
                $respons["error"] = FALSE;
                $respons["message"] = "Successfuly delete file.";
            }else {
                // set response code - 404 Not Found
                http_response_code(404);            
                $respons["error"] = TRUE;
                $respons["message"] = "File not found.";
            }
        }else {            
            // set response code - 502 Bad Gateway
            http_response_code(502);    
            $respons["error"] = TRUE;
            $respons["message"] = "File tidak bisa dihapus";
        }
    }else {
        // set response code - 400 Bad Request
        http_response_code(400);            
        $respons["error"] = TRUE;
        $respons["message"] = "Bad Request";
    }
    
    echo json_encode($respons);
?>