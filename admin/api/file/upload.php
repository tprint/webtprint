<?php
    // required headers
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");

    // include database and object files
    include_once '../config/database.php';
    include_once '../objects/file.php';
    include_once '../objects/storage.php';
    include_once '../objects/id_generator.php';
    
    // instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();
    
    // initialize object
    $file = new File($db);
    $storage = new Storage($db);
    $id_generator = new IDGenerator($db);
    $path = "../../upload/user_file";
    $respons = array();

    if (isset($_POST['id_user']) && $_FILES['file']['error'] === UPLOAD_ERR_OK) {
        $id_user = htmlspecialchars(substr($_POST['id_user'], 1, 8));
        $dir = htmlspecialchars($_FILES['file']['tmp_name']);
        $filesize = htmlspecialchars($_FILES['file']['size']);
        $filename = htmlspecialchars($_FILES['file']['name']);

        if (!is_dir($path . "/" . $id_user)) {
            mkdir($path . "/" . $id_user, 0777, true);
        }
        if ($storage->read($id_user)->rowCount() == 0) {
            $storage->create($id_user);
        }
        $user_storage = $storage->read($id_user)->fetch(PDO::FETCH_ASSOC); 
        $filepath = $path . '/' . $id_user . '/' . $filename;
        
        // initialize file data
        $file->id_user = $id_user;
        $file->nama = $filename;
        $file->halaman = 1;
        $file->size = $filesize;

        // check if file size too large or storage over capacity
        if (($filesize > 20000000) || ($user_storage['size'] + $filesize > 20000000)) {
            // set response code - 414 Request-URI Too Long
            http_response_code(414);
            
            $respons["error"] = TRUE;
            $respons["message"] = "File too large!";
        }else{
            if (move_uploaded_file($dir, $filepath)) {
                if (!$file->isExist($id_user, $filename)) {
                    $file->id_file = $id_generator->file();
                    if($file->save()){
                        $storage->update($id_user, $user_storage['size'] + $filesize);
                        // set response code - 201 Created
                        http_response_code(201);                      
                        $respons["error"] = FALSE;
                        $respons["message"] = "Upload file " . $filename . ' to ' . $filepath . ' berhasil';
                    }else{                        
                        // set response code - 502 Bad Gateway
                        http_response_code(502);
                        $respons["error"] = TRUE;
                        $respons["message"] = "Failed to insert to database!";
                    }
                }else{
                    $old_size = $file->getSize($id_user, $filename);
                    $storage->update($id_user, ($user_storage['size'] - $old_size));
                    $file->update();
                    $storage->update($id_user, ($user_storage['size'] + $filesize));
                    
                    // set response code - 202 Accepted
                    http_response_code(202);                      
                    $respons["error"] = FALSE;
                    $respons["message"] = "Update file " . $old_size . ' to ' . $filepath . ' berhasil';
                }
            } else {
                // set response code - 404 Not Found
                http_response_code(404);    
                $respons["error"] = TRUE;
                $respons["message"] = "Upload file failed! path = " . $filepath;
            }          
        }
    } else {
        // set response code - 400 Bad Request
        http_response_code(400);            
        $respons["error"] = TRUE;
        $respons["message"] = "Bad Request";

    }
    
    echo json_encode($respons);
?>