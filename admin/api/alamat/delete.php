<?php
    // required headers
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");

    // include database and object files
    include_once '../config/database.php';
    include_once '../objects/alamat.php';

    // instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();

    // initialize object
    $alamat = new Alamat($db);
    $respons = array();

    if(isset($_POST['id_user']) && isset($_POST['id_alamat'])){        
        $alamat->id_alamat = htmlspecialchars($_POST['id_alamat']);
        $alamat->id_user = htmlspecialchars($_POST['id_user']);
        if ($alamat->delete()) {
            // set response code - 200 OK
            http_response_code(200);            
            $respons["error"] = FALSE;
            $respons["message"] = "Successfuly delete data.";        
        }else {
            // set response code - 400 Bad Request
            // set response code - 502 Bad Gateway
            http_response_code(502);    
            $respons["error"] = TRUE;
            $respons["message"] = "delete data failed!";
        }
    }else {
        // set response code - 400 Bad Request
        http_response_code(400);    
        $response['error'] = true;
        $response['message'] = 'Required parameters are not available';
        // show products data in json format
    }
    
    echo json_encode($respons);
?>