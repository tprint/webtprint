<?php
    // required headers
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");

    // include database and object files
    include_once '../config/database.php';
    include_once '../objects/storage.php';
    
    // instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();
    
    // initialize object
    $storage = new Storage($db);
    $path = "../../upload/user_file";
    $respons = array();

    if(isset($_POST['id_user'])){
        $folder_name = $_POST['id_user'];

        if (!is_dir($path . "/" . $folder_name)) {
            if(mkdir($path . "/" . $folder_name, 0777, true)){
                $storage->create($folder_name);

                // set response code - 201 Created
                http_response_code(201);          

                $respons["error"] = FALSE;
                $respons["message"] = "Create folder $folder_name success.";
            }else{                
                // set response code - 502 Bad Gateway
                http_response_code(502);    
                
                $respons["error"] = FALSE;
                $respons["message"] = "Create folder $folder_name failed.";
            }
        }else{
            // set response code - 403 Forbidden
            http_response_code(403);
        
            $respons["error"] = FALSE;
            $respons["message"] = "Folder $folder_name already exist.";
        }
    }
    echo json_encode($respons);
?>