<?php
    // required headers
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");

    // include database and object files
    include_once '../config/database.php';
    include_once '../objects/storage.php';
    
    // instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();
    
    // initialize object
    $storage = new Storage($db);
    $path = "../../upload/user_file";
    $respons = array();

    if(isset($_POST['id_user']) && !empty($_POST['id_user'])){
        $id_user = $_POST['id_user'];
        $stmt = $storage->read($id_user);
        $row = $stmt->rowCount();
        
        if ($row > 0) {
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                extract($row);
                
                $respons['id_folder'] = $id_folder;
                $respons['id_user'] = $id_user;
                $respons['size'] = $size;
                $respons['max_size'] = $max_size;
                $respons['error'] = FALSE;
                $respons['message'] = "Storage found.";
            }
            // set response code - 200 OK
            http_response_code(200);            
            // show products data in json format
            echo json_encode($respons);
        }else{            
            // set response code - 404 Not found
            http_response_code(404);
            
            $respons["error"] = TRUE;
            $respons["message"] = "Storage not found.";

            // tell the user no products found
            echo json_encode($respons);
        }
    }
?>