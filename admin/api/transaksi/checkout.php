<?php
    // required headers
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");

    // include database and object files
    include_once '../config/database.php';
    include_once '../objects/transaksi.php';
    include_once '../objects/id_generator.php';

    // instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();

    // initialize object
    $transaksi = new Transaksi($db);
    $id_generator = new IDGenerator($db);
    $response = array();

    if(isset($_POST['user']) && isset($_POST['maps']) && isset($_POST['mitra']) && isset($_POST['produk'])){
        $transaksi->id_transaksi=$id_generator->transaction();
        $transaksi->id_user=htmlspecialchars($_POST['user']);
        $transaksi->id_mitra=htmlspecialchars($_POST['mitra']);
        $transaksi->id_produk=htmlspecialchars($_POST['produk']);
        $transaksi->id_maps=htmlspecialchars($_POST['maps']);
        $transaksi->id_file=htmlspecialchars($_POST['file']);
        $transaksi->paper_qty=htmlspecialchars($_POST['paper']);
        $transaksi->copy_qty=htmlspecialchars($_POST['copy']);
        $transaksi->warna=htmlspecialchars($_POST['warna']);
        $transaksi->ongkir=htmlspecialchars($_POST['ongkir']);
        $transaksi->harga_satuan=htmlspecialchars($_POST['harga']);
        $transaksi->harga_total=htmlspecialchars($_POST['total']);
        $transaksi->keterangan=htmlspecialchars($_POST['keterangan']);
        
        //query transaksi
        $stmt = $transaksi->checkout();
        
        if ($stmt) {
            $response['error'] = false;
            $response['message'] = 'Transaction success';
            // set response code - 200 OK
            http_response_code(200);    
            // show products data in json format
            echo json_encode($response);
        }else{
            // set response code - 502 Bad Gateway
            http_response_code(502);
            $response['error'] = true;
            $response['message'] = 'Transaction failed';    
            // show products data in json format
            echo json_encode($response);
        }
    }else{
        // set response code - 400 Bad Request
        http_response_code(400);    
        $response['error'] = true;
        $response['message'] = 'Required parameters are not available';
        // show products data in json format
        echo json_encode($response);
    }
?>