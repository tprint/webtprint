<?php
// required headers
    //header("Access-Control-Allow-Origin: http://localhost/EZPrint/admin/api/");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 

    // include database and object files
    include_once '../config/database.php';
    include_once '../objects/transaksi.php';

    // instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();

    // initialize object
    $transaksi = new Transaksi($db);
    $response = array();

    if (isset($_POST['id'])) {
        $transaksi->id_transaksi = htmlspecialchars($_POST['id']);

        $stmt = $transaksi->konfirm();

        if ($stmt) {
            $response['error'] = false;
            $response['message'] = 'Update data success';
            // set response code - 200 OK
            http_response_code(200);    
            // show products data in json format
            echo json_encode($response);
        }else{
            // set response code - 502 Bad Gateway
            http_response_code(502);
            $response['error'] = true;
            $response['message'] = 'Update data failed';    
            // show products data in json format
            echo json_encode($response);
        }
        
    } else {
        // set response code - 400 Bad Request
        http_response_code(400);    
        $response['error'] = true;
        $response['message'] = 'Required parameters are not available';
        // show products data in json format
        echo json_encode($response);
    }
?>