<?php
    // required headers
    //header("Access-Control-Allow-Origin: http://localhost/EZPrint/admin/api/");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 

    // include database and object files
    include_once '../config/database.php';
    include_once '../objects/user.php';
    include_once '../objects/id_generator.php';
    
    // instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();

    //initialize object
    $user = new User($db);
    $id_generator = new IDGenerator($db);
    $user_arr = array();

    if (isset($_POST['email']) && isset($_POST['password']) && isset($_POST['nama']) && isset($_POST['telp'])) {
        $user->email = htmlspecialchars($_POST['email']);
        $user->password = md5(htmlspecialchars($_POST['password']));                
        $user->nama = htmlspecialchars($_POST['nama']);
        $user->telp = htmlspecialchars($_POST['telp']);

        // Check jika email sudah terdaftar
        if(!$user->emailExists()){
            $new_id = $id_generator->user();
            if ($user->register($new_id)) {
                $stmt = $user->login();
                
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    extract($row);
                    
                    $user_arr["id_user"] = $id_user;
                    $user_arr["nama"] = $nama;
                    $user_arr["email"] = $email;
                    $user_arr["telp"] = $telp;
                    $user_arr["password"] = $password;
                    $user_arr["status"] = $status;
                    $user_arr["last_login"] = $last_login;
                    $user_arr["error"] = FALSE;
                    $user_arr["message"] = "Register sukses!";
                }
    
                // set response code - 200 OK
                http_response_code(200);
            
                // show products data in json format
                echo json_encode($user_arr);
            }else{
                // set response code - 400 Bad Request
                http_response_code(400);
                
                $user_arr["error"] = TRUE;
                $user_arr["message"] = "Register gagal! ID : " . $new_id;

                // tell the user no products found
                echo json_encode($user_arr);
            }
        }else{            
            // set response code - 403 Forbiden
            http_response_code(403);
            
            $user_arr["error"] = TRUE;
            $user_arr["message"] = "Email atau Nomor Telephon sudah terdaftar!";

            // tell the user no products found
            echo json_encode($user_arr);
        }
    }
?>