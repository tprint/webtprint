<?php
    class Transaksi{
        private $conn;
        private $table_name = "tbl_transaksi";

        //object properties 
        public $id_transaksi;
        public $id_user;
        public $id_mitra;
        public $id_produk;
        public $id_file;
        public $maps_dest;
        public $status;
        public $tgl_pesan;
        public $tgl_delivery;
        public $tgl_selesai;
        public $paper_qty;
        public $copy_qty;
        public $warna;
        public $ongkir;
        public $harga_satuan;
        public $harga_total;
        public $keterangan;

        // constructor with $db as database connection
        public function __construct($db){
            $this->conn = $db;
        }

        //read transaksi
        public function read(){
            //select all query
            $query = "SELECT * FROM " . $this->table_name . " WHERE id_user=:id_user";

            //perpare query
            $stmt = $this->conn->prepare($query);

            //sanitize
            $this->id_user=htmlspecialchars(strip_tags($this->id_user));

            //bind given value
            $stmt->bindparam(":id_user", $this->id_user);

            $stmt->execute();

            return $stmt;
        }

        public function konfirm()
        {
            date_default_timezone_set("Asia/Jakarta");
            $query = "UPDATE tbl_transaksi SET status='2', tgl_selesai='". date('Y-m-d H:i:s')."' WHERE id_transaksi=:id";

            $stmt = $this->conn->prepare($query);

            $this->id_transaksi=htmlspecialchars(strip_tags($this->id_transaksi));
            
            $stmt->bindParam(":id", $this->id_transaksi);
            
            // execute query
            if($stmt->execute()){
                return true;
            }
            return false;
        }
        
        //insert to transaksi
        public function checkout(){
            $query = "INSERT INTO ".$this->table_name." SET 
                    id_transaksi=:id,
                    id_user=:user, 
                    id_mitra=:mitra, 
                    id_produk=:produk,
                    id_file=:file,
                    id_maps=:maps_dest,
                    paper_qty=:paper,
                    copy_qty=:copy,
                    warna=:warna,
                    ongkir=:ongkir,
                    harga_satuan=:harga_satuan,
                    harga_total=:harga_total,
                    keterangan=:ket";

            // prepare query
            $stmt = $this->conn->prepare($query);
        
            // sanitize
            $this->id_transaksi=htmlspecialchars(strip_tags($this->id_transaksi));
            $this->id_user=htmlspecialchars(strip_tags($this->id_user));
            $this->id_mitra=htmlspecialchars(strip_tags($this->id_mitra));
            $this->id_produk=htmlspecialchars(strip_tags($this->id_produk));
            $this->id_file=htmlspecialchars(strip_tags($this->id_file));
            $this->maps_dest=htmlspecialchars(strip_tags($this->maps_dest));
            $this->paper_qty=htmlspecialchars(strip_tags($this->paper_qty));
            $this->copy_qty=htmlspecialchars(strip_tags($this->copy_qty));
            $this->warna=htmlspecialchars(strip_tags($this->warna));
            $this->ongkir=htmlspecialchars(strip_tags($this->ongkir));
            $this->harga_satuan=htmlspecialchars(strip_tags($this->harga_satuan));
            $this->harga_total=htmlspecialchars(strip_tags($this->harga_total));
            $this->keterangan=htmlspecialchars(strip_tags($this->keterangan));
        
            // bind values
            $stmt->bindParam(":id", $this->id_transaksi);
            $stmt->bindParam(":user", $this->id_user);
            $stmt->bindParam(":mitra", $this->id_mitra);
            $stmt->bindParam(":produk", $this->id_produk);
            $stmt->bindParam(":file", $this->id_file);
            $stmt->bindParam(":maps_dest", $this->id_maps);
            $stmt->bindParam(":paper", $this->paper_qty);
            $stmt->bindParam(":copy", $this->copy_qty);
            $stmt->bindParam(":warna", $this->warna);
            $stmt->bindParam(":ongkir", $this->ongkir);
            $stmt->bindParam(":harga_satuan", $this->harga_satuan);
            $stmt->bindParam(":harga_total", $this->harga_total);
            $stmt->bindParam(":ket", $this->keterangan);
        
            // execute query
            if($stmt->execute()){
                return true;
            }
        
            return false;
        }
    }
?>