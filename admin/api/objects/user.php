<?php
    class User{
        private $conn;
        private $table_name = "tbl_user";

        //object properties
        public $id_user;
        public $nama;
        public $email;
        public $password;
        public $telp;
        public $status;
        public $last_login;

        // constructor with $db as database connection
        public function __construct($db){
            $this->conn = $db;
        }

        public function getUsername($id)
        {
            $username = "";
            $query = "SELECT nama FROM " . $this->table_name ." WHERE id_user=".$id." LIMIT 0,1";

            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                extract($row);
                $username = $nama;
            }
            return $username;
        }

        // check if given email exist in the database
        function emailExists(){        
            // query to check if email exists
            $query = "SELECT * FROM " . $this->table_name . "
                    WHERE email=:email OR telp=:telp
                    LIMIT 0,1";
        
            // prepare the query
            $stmt = $this->conn->prepare( $query );
        
            // sanitize
            $this->email=htmlspecialchars(strip_tags($this->email));
            $this->telp=htmlspecialchars(strip_tags($this->telp));
        
            // bind given email value
            $stmt->bindParam(':email', $this->email);
            $stmt->bindParam(':telp', $this->telp);
        
            // execute the query
            $stmt->execute();
        
            // get number of rows
            $num = $stmt->rowCount();
        
            // if email exists, assign values to object properties for easy access and use for php sessions
            if($num>0){
                // return true because email exists in the database
                return true;
            }        
            // return false if email does not exist in the database
            return false;
        }

        //login user
        public function login()
        {
            $query = "SELECT * FROM ". $this->table_name ." 
                    WHERE email=:username OR telp=:username AND password=:password";
            
            //prepare query 
            $stmt = $this->conn->prepare($query);

            // sanitize
            $this->email=htmlspecialchars(strip_tags($this->email));
            $this->telp=htmlspecialchars(strip_tags($this->telp));
            $this->password=htmlspecialchars(strip_tags($this->password));
        
            // bind given email value
            $stmt->bindParam(":username", $this->email);
            $stmt->bindParam(":username", $this->telp);
            $stmt->bindParam(":password", $this->password);

            $stmt->execute();

            return $stmt;
        }

        //register new user
        public function register($id)
        {
            $query = "INSERT INTO " . $this->table_name . " SET id_user='$id', nama=:nama, email=:email, telp=:telp, password=:password";
            // prepare query
            $stmt = $this->conn->prepare($query);
        
            // sanitize
            $this->nama=htmlspecialchars(strip_tags($this->nama));
            $this->telp=htmlspecialchars(strip_tags($this->telp));
            $this->email=htmlspecialchars(strip_tags($this->email));
            $this->password=htmlspecialchars(strip_tags($this->password));
        
            // bind values
            $stmt->bindParam(":nama", $this->nama);
            $stmt->bindParam(":email", $this->email);
            $stmt->bindParam(":telp", $this->telp);
            $stmt->bindParam(":password", $this->password);
        
            // execute query
            if($stmt->execute()){
                return true;
            }
        
            return false;
        }

        //get all user
        public function read()
        {
            $query = "SELECT * FROM ". $this->table_name;
            
            //prepare query 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            return $stmt;
        }
    }
?>