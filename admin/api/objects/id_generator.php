<?php
	class IDGenerator{
		private $conn;
		// constructor with $db as database connection
        public function __construct($db){
            $this->conn = $db;
        }

		function mitra()
		{
			$query="SELECT max(id_mitra) as maxKode FROM tbl_mitra";
			$result = $this->conn->prepare($query);
			$result->execute();
			$data = $result->fetch(PDO::FETCH_ASSOC);
			$kodeMitra = substr($data['maxKode'], 4, 4);
			$tahun=date("Y");

			$year=(int) substr($tahun,2,2);
			

			$noUrut = (int)($kodeMitra);

			$noUrut++;

			$kodeMitra ="MX".$year. sprintf("%04s", $noUrut);

			return $kodeMitra;
		}

		function file()
		{
			$query="SELECT max(id_file) as maxKode FROM tbl_file";
			$result = $this->conn->prepare($query);
			$result->execute();
			$data = $result->fetch(PDO::FETCH_ASSOC);
			$kodeFile = substr($data['maxKode'], 4, 4);
			$tahun=date("Y");

			$year=(int) substr($tahun,2,2);
			

			$noUrut = (int)($kodeFile);

			$noUrut++;

			$kodeFile ="FX".$year. sprintf("%04s", $noUrut);
			return $kodeFile;
		}

		function user()
		{
			$query="SELECT max(id_user) as maxKode FROM tbl_user";
			$result = $this->conn->prepare($query);
			$result->execute();
			$data = $result->fetch(PDO::FETCH_ASSOC);
			$kodeUser = substr($data['maxKode'], 4, 4);
			$tahun=date("Y");

			$year=(int) substr($tahun,2,2);
			

			$noUrut = (int)($kodeUser);

			$noUrut++;

			$kodeUser ="UX".$year. sprintf("%04s", $noUrut);
			return $kodeUser;
		}

		function transaction()
		{
			$query="SELECT max(id_transaksi) as maxKode FROM tbl_transaksi";
			$result = $this->conn->prepare($query);
			$result->execute();
			$data = $result->fetch(PDO::FETCH_ASSOC);
			$kodeTransaksi = substr($data['maxKode'], 8, 4);
			$tahun=date("Y");

			$year=(int) substr($tahun,2,2);
			

			$noUrut = (int)($kodeTransaksi);

			$noUrut++;

			$kodeTransaksi ="TX".$year.date("md"). sprintf("%04s", $noUrut);
			return $kodeTransaksi;
		}

		function promotion()
		{
			$query="SELECT max(id_promo) as maxKode FROM tbl_promo";
			$result = $this->conn->prepare($query);
			$result->execute();
			$data = $result->fetch(PDO::FETCH_ASSOC);
			$kodePromo = substr($data['maxKode'], 6, 2);
			$tahun=date("Y");

			$year=(int) substr($tahun,2,2);
			

			$noUrut = (int)($kodePromo);

			$noUrut++;

			$kodePromo ="PX".$year.date("m"). sprintf("%02s", $noUrut);
			return $kodePromo;
		}

		function bundle()
		{
			$query="SELECT max(id_bundle) as maxKode FROM tbl_bundle";
			$result = $this->conn->prepare($query);
			$result->execute();
			$data = $result->fetch(PDO::FETCH_ASSOC);
			$kodeBundle = substr($data['maxKode'], 4, 4);
			$tahun=date("Y");

			$year=(int) substr($tahun,2,2);
			

			$noUrut = (int)($kodeBundle);

			$noUrut++;

			$kodeBundle ="BX".$year. sprintf("%04s", $noUrut);
			return $kodeBundle;
		}

	}
?>