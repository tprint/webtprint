<?php
    class Mitra{
        private $conn;
        private $table_name = "tbl_mitra";

        //object properties
        public $id_mitra;
        public $nama;
        public $email;
        public $telp;
        public $logo;
        public $alamat;
        public $provinsi;
        public $kota;
        public $kecamatan;
        public $kodepos;
        public $id_maps;

        // constructor with $db as database connection
        public function __construct($db){
            $this->conn = $db;
        }

        //read mitra
        public function read(){
            //select all query
            $query = "SELECT * FROM tbl_mitra, tbl_alamat
                        WHERE tbl_alamat.id_mitra=tbl_mitra.id_mitra";

            //prepare query 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            return $stmt;
        }
    }
?>