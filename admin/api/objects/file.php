<?php
    class File{
        private $conn;
        private $table_name = "tbl_file";
        private $path = "../../upload/user_file";

        //object properties
        public $id_file;
        public $id_user;
        public $nama;
        public $halaman;
        public $size;

        // constructor with $db as database connection
        public function __construct($db){
            $this->conn = $db;
        }

        //read file
        public function read(){
            //select all query
            $query = "SELECT * FROM " . $this->table_name . " WHERE id_user=:id_user";

            //perpare query
            $stmt = $this->conn->prepare($query);

            //sanitize
            $this->id_user=htmlspecialchars(strip_tags($this->id_user));

            //bind given value
            $stmt->bindparam(":id_user", $this->id_user);

            $stmt->execute();

            return $stmt;
        }

        //check file is already uploaded ?
        public function isExist($id_user, $file_name){
            $query = "SELECT * FROM " . $this->table_name . " WHERE id_user='$id_user' AND nama='$file_name'";

            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            // get number of rows
            $num = $stmt->rowCount();
        
            // if email exists, assign values to object properties for easy access and use for php sessions
            if($num>0){
                // return true because email exists in the database
                return true;
            }        
            // return false if email does not exist in the database
            return false;
        }

        //upload file to server
        public function save(){
            $query = "INSERT INTO " . $this->table_name . " SET 
                        id_file=:id,
                        id_user=:id_user,
                        nama=:file,
                        halaman=:halaman,
                        size=:size";

            // prepare query
            $stmt = $this->conn->prepare($query);
        
            // sanitize
            $this->id_file=htmlspecialchars(strip_tags($this->id_file));
            $this->id_user=htmlspecialchars(strip_tags($this->id_user));
            $this->nama=htmlspecialchars(strip_tags($this->nama));
            $this->halaman=htmlspecialchars(strip_tags($this->halaman));
            $this->size=htmlspecialchars(strip_tags($this->size));
        
            // bind values
            $stmt->bindParam(":id", $this->id_file);
            $stmt->bindParam(":id_user", $this->id_user);
            $stmt->bindParam(":file", $this->nama);
            $stmt->bindParam(":halaman", $this->halaman);
            $stmt->bindParam(":size", $this->size);
        
            // execute query
            if($stmt->execute()){
                return true;
            }
        
            return false;
        }

        //update file size for upload existing file
        public function update()
        {
            $query = "UPDATE " . $this->table_name . 
                    " SET size=:size, halaman=:halaman 
                    WHERE id_file=:id_file AND nama=:nama";

            // prepare query
            $stmt = $this->conn->prepare($query);
                    
            // sanitize
            $this->id_file=htmlspecialchars(strip_tags($this->id_file));
            $this->nama=htmlspecialchars(strip_tags($this->nama));
            $this->halaman=htmlspecialchars(strip_tags($this->halaman));
            $this->size=htmlspecialchars(strip_tags($this->size));

            // bind values
            $stmt->bindParam(":id_file", $this->id_file);
            $stmt->bindParam(":nama", $this->nama);
            $stmt->bindParam(":halaman", $this->halaman);
            $stmt->bindParam(":size", $this->size);

            // execute query
            if($stmt->execute()){
                return true;
            }

            return false;
        }

        public function delete()
        {
            $query = "DELETE FROM " . $this->table_name . " WHERE id_user=:id_user AND nama=:nama";
            
            // prepare query
            $stmt = $this->conn->prepare($query);
                    
            // sanitize
            $this->id_user=htmlspecialchars(strip_tags($this->id_user));
            $this->nama=htmlspecialchars(strip_tags($this->nama));

            // bind values
            $stmt->bindParam(":id_user", $this->id_user);
            $stmt->bindParam(":nama", $this->nama);

            // execute query
            if($stmt->execute()){
                return true;
            }

            return false;
        }

        public function getSize($id_file)
        {
            $fsize = 0;
            $query = "SELECT size FROM tbl_file WHERE id_file='".$id_file."' LIMIT 0,1";
            // prepare query
            $stmt = $this->conn->prepare($query);
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                extract($row);
                $fsize = $size;
            }
            return $fsize;
        }
    }
?>