<?php
    class Promo{
        private $conn;
        private $table_name = "tbl_promo";

        //objects properties
        public $id_promo;
        public $id_mitra;
        public $nama;
        public $banner;
        public $detail;
        public $dateStart;
        public $dateEnd;
        
        // constructor with $db as database connection
        public function __construct($db){
            $this->conn = $db;
        }

        // read promo
        public function read(){
            $query = "SELECT * FROM tbl_promo WHERE archive=0 AND payment_status=1";

            //prepare query 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            return $stmt;
        }
    }
?>