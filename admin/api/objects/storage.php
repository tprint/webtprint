<?php
    class Storage{
        private $conn;
        private $table_name = "tbl_folder";

        //object properties
        public $id_folder;
        public $id_user;
        public $size;
        public $max_size;

        // constructor with $db as database connection
        public function __construct($db){
            $this->conn = $db;
        }

        //read storage
        public function read($id_user)
        {
            //select all query
            $query = "SELECT * FROM " . $this->table_name . " WHERE id_user='$id_user' LIMIT 0,1";

            //prepare query 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            return $stmt;
        }

        public function create($id_user)
        {
            //select all query
            $query = "INSERT INTO " . $this->table_name . " (id_user) VALUES ('$id_user')";

            //prepare query 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            return $stmt;
        }

        public function update($id_user, $size)
        {
            //select all query
            $query = "UPDATE " . $this->table_name . " SET size='$size' WHERE id_user='$id_user'";

            //prepare query 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            // execute query
            if($stmt->execute()){
                return true;
            }

            return false;
        }
    }
?>