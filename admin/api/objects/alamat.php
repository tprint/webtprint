<?php
    class Alamat{
        private $conn;
        private $table_name = "tbl_alamat_user";

        //object properties 
        public $id_alamat;
        public $id_user;
        public $nama;
        public $alamat;
        public $provinsi;
        public $kota;
        public $kecamatan;
        public $kodepos;

        // constructor with $db as database connection
        public function __construct($db){
            $this->conn = $db;
        }

        public function read()
        {
            $query = "SELECT * FROM ".$this->table_name." WHERE id_user=:id_user";

            $stmt = $this->conn->prepare($query);
            
            //sanitize
            $this->id_user=htmlspecialchars(strip_tags($this->id_user));

            //bind given value
            $stmt->bindparam(":id_user", $this->id_user);

            $stmt->execute();

            return $stmt;
        }

        public function add()
        {
            $query = "INSERT INTO ".$this->table_name." SET 
                        id_user=:user,
                        nama=:nama,
                        alamat=:alamat,
                        provinsi=:prov,
                        kota=:kota,
                        kecamatan=:kec,
                        kode_post=:pos";
            
            $stmt = $this->conn->prepare($query);

            $this->id_user=htmlspecialchars(strip_tags($this->id_user));
            $this->nama=htmlspecialchars(strip_tags($this->nama));
            $this->alamat=htmlspecialchars(strip_tags($this->alamat));
            $this->provinsi=htmlspecialchars(strip_tags($this->provinsi));
            $this->kota=htmlspecialchars(strip_tags($this->kota));
            $this->kecamatan=htmlspecialchars(strip_tags($this->kecamatan));
            $this->kodepos=htmlspecialchars(strip_tags($this->kodepos));

            $stmt->bindParam(":user", $this->id_user);
            $stmt->bindParam(":nama", $this->nama);
            $stmt->bindParam(":alamat", $this->alamat);
            $stmt->bindParam(":prov", $this->provinsi);
            $stmt->bindParam(":kota", $this->kota);
            $stmt->bindParam(":kec", $this->kecamatan);
            $stmt->bindParam(":pos", $this->kodepos);

            // execute query
            if($stmt->execute()){
                return true;
            }
        
            return false;
        }

        public function update()
        {
            $query = "UPDATE ".$this->table_name." SET 
                        nama=:nama,
                        alamat=:alamat,
                        provinsi=:prov,
                        kota=:kota,
                        kecamatan=:kec,
                        kode_post=:pos
                        WHERE id_alamat=:id AND id_user=:user";
            
            $stmt = $this->conn->prepare($query);

            $this->id_alamat=htmlspecialchars(strip_tags($this->id_alamat));
            $this->id_user=htmlspecialchars(strip_tags($this->id_user));
            $this->nama=htmlspecialchars(strip_tags($this->nama));
            $this->alamat=htmlspecialchars(strip_tags($this->alamat));
            $this->provinsi=htmlspecialchars(strip_tags($this->provinsi));
            $this->kota=htmlspecialchars(strip_tags($this->kota));
            $this->kecamatan=htmlspecialchars(strip_tags($this->kecamatan));
            $this->kodepos=htmlspecialchars(strip_tags($this->kodepos));

            $stmt->bindParam(":id", $this->id_alamat);
            $stmt->bindParam(":user", $this->id_user);
            $stmt->bindParam(":nama", $this->nama);
            $stmt->bindParam(":alamat", $this->alamat);
            $stmt->bindParam(":prov", $this->provinsi);
            $stmt->bindParam(":kota", $this->kota);
            $stmt->bindParam(":kec", $this->kecamatan);
            $stmt->bindParam(":pos", $this->kodepos);

            // execute query
            if($stmt->execute()){
                return true;
            }
            return false;
        }

        public function delete()
        {
            $query = "DELETE FROM ".$this->table_name." WHERE id_alamat=:id AND id_user=:user";

            $stmt = $this->conn->prepare($query);

            $this->id_alamat=htmlspecialchars(strip_tags($this->id_alamat));
            $this->id_user=htmlspecialchars(strip_tags($this->id_user));
            
            $stmt->bindParam(":id", $this->id_alamat);
            $stmt->bindParam(":user", $this->id_user);
            
            // execute query
            if($stmt->execute()){
                return true;
            }
            return false;
        }
    }
?>
