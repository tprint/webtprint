<?php
    class Produk{
        private $conn;
        private $table_name = "tbl_produk";

        //object properties
        public $id_produk;
        public $id_mitra;
        public $id_kategori;
        public $ukuran;
        public $bahan;
        public $price_c;
        public $price_b;
        public $kategori;
        public $icon;
        
        // constructor with $db as database connection
        public function __construct($db){
            $this->conn = $db;
        }

        //read produk
        public function read(){
            //select all query
            $query = "SELECT * FROM tbl_produk, tbl_kategori 
                        WHERE tbl_kategori.id_kategori=tbl_produk.id_kategori";

            //perpare query
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            return $stmt;
        }
    }
?>