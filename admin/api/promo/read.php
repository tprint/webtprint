<?php
    // required headers
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");

    // include database and object files
    include_once '../config/database.php';
    include_once '../objects/promo.php';
    
    // instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();
    
    // initialize object
    $promo = new Promo($db);

    //query promo
    $stmt = $promo->read();
    $num = $stmt->rowCount();

    //check if more than 0 record found
    if($num > 0){
        //promo array
        $promo_arr = array();
        $promo_arr["promo"] = array();

        // retrieve our table contents
        // fetch() is faster than fetchAll()
        // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            // extract row
            // this will make $row['name'] to
            // just $name only
            extract($row);
    
            $promo_item=array(
                "id_promo" => $id_promo,
                "id_mitra" => $id_mitra,
                "nama" => $nama,
                "banner" => $banner,
                "detail" => $detail,
                "dateStart" => $tgl_berlaku,
                "dateEnd" => $tgl_selesai,
            );
    
            array_push($promo_arr["promo"], $promo_item);
        }
    
        // set response code - 200 OK
        http_response_code(200);
    
        // show products data in json format
        echo json_encode($promo_arr);
    }else{ 
        // set response code - 404 Not found
        http_response_code(404);
     
        // tell the user no products found
        echo json_encode(
            array("message" => "No promotion found.")
        );
    }
?>