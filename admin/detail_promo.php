<?php
	$page = 'Promo';
	$icon_loct = "upload/promosi";
	include 'core/init.php';
    include 'include/header.php';
	include 'include/sidebar.php';
	
    session_start();
    if(!isset($_SESSION['loginadmin'])) {
      header('location:login.php');
    }else {
      $loginadmin = $_SESSION['loginadmin'];
	}
	
	//Query Select Data Transaksi
	$query="SELECT id_promo, 
			p.id_mitra,
            p.nama, 
            banner, 
            tipe_bundle, 
            tgl_berlaku, 
            tgl_selesai, 
            detail,
            price,
            tax, 
            total,
			tgl_berlaku,
			tgl_selesai,
			payment_status AS payment,
			tipe_bundle AS bundle,
            m.nama AS mitra
            FROM tbl_promo p, tbl_mitra m 
            WHERE m.id_mitra=p.id_mitra 
            AND id_promo='".$_GET['id']."'";
	$result = $conn->query($query) or die($conn->error);
	$row = mysqli_fetch_assoc( $result );
?>
	<!-- Content -->
	<div class="main-panel">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-6">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="card-icon col-md-2 text-rose">
										<i class='material-icons'>
											<?php echo (($row['payment']=='0')? 'shopping_basket' : 'cloud_done');?>
										</i>
									</div>
									<h4 class="card-title">
										<?php echo (($row['payment']=='0')? 'Belum Dibayar' : 'Pembayaran Lunas');?>
									</h4>
								</div>
							</div> <!--End card body-->
						</div> <!-- End card-->
					</div>
					<div class="col-lg-4 col-md-4 col-sm-6">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="card-icon col-md-2 text-rose">
										<i class='material-icons'>store</i>
									</div>
									<a href="mitra_detail.php?id=<?php echo $row['id_mitra'];?>"><h4 class="card-title"><?php echo $row['mitra']; ?></h4></a>
								</div>
							</div> <!--End card body-->
						</div> <!-- End card-->
					</div>
				</div>
				<div class="row">
					<!-- Start Card Shipping Destination-->
					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="card">
							<img class="card-image-top" height="300px" src="<?php echo $icon_loct .'/'. $row['banner'];?>"/>
							<!-- <div class="card-header card-header-rose">
								<h4 class="card-title">Alamat Pengiriman</h4>
							</div> -->
							<div class="card-body">
								<h4><?php echo $row['nama']; ?></h4>
								<p><?php echo $row['detail']; ?></p>
							</div>
						</div>
					</div> <!-- End Card Shipping Destination-->
					<!-- Start Card Order Detail-->
					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="card">
							<div class="card-header card-header-rose">
								<h4 class="card-title">Detail Pemesanan</h4>
							</div>
							<div class="card-body">
								<!-- Start Table Produk-->
								<div class="table-responsive">
									<table class="table table-shopping">
										<thead class="text-danger text-center">
											<tr>
												<th>Bundle</th>
												<th>Start Date</th>
												<th>End Date</th>
											</tr>
										</thead>
										<tbody>
												<tr  class="text-center">
													<td><?php echo $row['bundle']; ?></td>
													<td><?php echo $row['tgl_berlaku']; ?></td>
													<td><?php echo $row['tgl_selesai']; ?></td>
												</tr>
										</tbody>
									</table>
								</div> <!--End Table Produk-->
								<hr/>
								<!-- Start Table Price Harga-->
								<div class="col-md-8 pull-right table-responsive">
									<table class="table table-shopping">
										<tbody class="text-right">
											<tr>
												<td>Harga : </td>
												<td>Rp. <?php echo number_format($row['price']);?></td>
											</tr>
											<tr>
												<td>Pajak 10% : </td>
												<td>Rp. <?php echo number_format($row['tax']);?></td>
											</tr>
											<tr>
												<td>Total : </td>
												<td class="td-name">Rp. <?php echo number_format($row['total']);?></td>
											</tr>
										</tbody>
									</table>
								</div>
								<!--End Table-->
							</div>
						</div>
					</div><!-- End Card Order Detail-->
				</div>
			</div> <!-- End container fluid-->
	</div>
</body>
<?php
    include 'include/footer.php';
?>
</html>