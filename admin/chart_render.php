<?php
    include 'chart_data.php';
?>

<script>
    // Set new default font family and font color to mimic Bootstrap's default styling
    Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
    Chart.defaults.global.defaultFontColor = '#292b2c';

    // Area Chart Example
    var ctx = document.getElementById("myAreaChart");
    var myLineChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["January", 
                    "February", 
                    "March", 
                    "April", 
                    "May", 
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"],
        datasets: [{
        label: "Transaction",
        lineTension: 0.3,
        backgroundColor: "rgba(2,117,216,0.2)",
        borderColor: "rgba(2,117,216,1)",
        pointRadius: 5,
        pointBackgroundColor: "rgba(2,117,216,1)",
        pointBorderColor: "rgba(255,255,255,0.8)",
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgba(2,117,216,1)",
        pointHitRadius: 1,
        pointBorderWidth: 2,
        data: [<?php echo $jan ?>,
                        <?php echo $feb ?>,
                        <?php echo $mar ?>,
                        <?php echo $apr ?>,
                        <?php echo $may ?>,
                        <?php echo $jun ?>,
                        <?php echo $jul ?>,
                        <?php echo $aug ?>,
                        <?php echo $sep ?>,
                        <?php echo $oct ?>,
                        <?php echo $nov ?>,
                        <?php echo $dec ?>],
        }],
    },
    options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

</script>

<script>
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ["January", 
                    "February", 
                    "March", 
                    "April", 
                    "May", 
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"],
            datasets: [{
                data: [<?php echo $jan ?>,
                        <?php echo $feb ?>,
                        <?php echo $mar ?>,
                        <?php echo $apr ?>,
                        <?php echo $may ?>,
                        <?php echo $jun ?>,
                        <?php echo $jul ?>,
                        <?php echo $aug ?>,
                        <?php echo $sep ?>,
                        <?php echo $oct ?>,
                        <?php echo $nov ?>,
                        <?php echo $dec ?>],
                borderColor: ['rgba(255,17,17,1)'],
                backgroundColor: ['rgba(255, 17, 17, 0.2)'],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
</script>