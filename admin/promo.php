<?php
	$page = 'Promo';

    // session
    session_start();
    if(!isset($_SESSION['loginadmin'])) {
      header('location:login.php');
    }else {
      $loginadmin = $_SESSION['loginadmin'];
    }

	include 'core/init.php';
    include 'include/header.php';
    include 'include/sidebar.php';
    include 'include/navbar.php';
    include_once 'api/config/database.php';
    include_once 'api/objects/id_generator.php';

    // instantiate database
    $database = new Database();
    $db = $database->getConnection();

    //initialize object
    $id_generator = new IDGenerator($db);
    
    $id = 0;
    $id_bundle = "";
    $mode = "";
    $status = 0;
    $icon_loct = "upload/promosi/";

    if (isset($_GET['id'])) {
        $id = $_GET['id'];
    }
    if (isset($_GET['bundle'])) {
        $id_bundle = $_GET['bundle'];
    }
    if (isset($_GET['mode'])) {
        $mode = $_GET['mode'];
    }
    if (isset($_GET['status'])) {
        $status = $_GET['status'];
    }
    
    $q_promo = "SELECT id_promo, p.nama, banner, tipe_bundle, tgl_berlaku, tgl_selesai, m.nama AS mitra
                FROM tbl_promo p, tbl_mitra m 
                WHERE m.id_mitra=p.id_mitra AND payment_status='".$status."'";
    $promo=$conn->query($q_promo); 

    $q_bundle = "SELECT * FROM tbl_bundle";
    $bundle = $conn->query($q_bundle);

    if ($mode == "edit"){
        $query = "SELECT * FROM tbl_bundle WHERE id_bundle='".$id_bundle."'";
        $result_edit = $conn->query($query);
    }

    if (isset($_POST['save'])) {
        if($mode == "edit"){
            $query = "UPDATE tbl_bundle SET nama='".$_POST['nama']."', price='".$_POST['harga']."' 
                        WHERE id_bundle='".$id_bundle."'";
            if($conn->query($query)){
                echo "<script>alert('Data sucessfully saved !');</script>";
                echo "<script>location='promo.php';</script>";
            }else{
                echo "<script>alert('Gagal mengubah data.');</script>";
            }
        }else{
            $id_bundle = $id_generator->bundle();
            $query = "INSERT INTO tbl_bundle SET id_bundle='".$id_bundle."', nama='".$_POST['nama']."', price=".$_POST['harga'];
            if($conn->query($query)){
                echo "<script>alert('Data sucessfully saved !');</script>";
                echo "<script>location='promo.php';</script>";
            }else{
                echo "<script>alert('Gagal menambahkan data.');</script>";
            }
        }
    }

    if (isset($_GET['paid'])) {
        $query = "UPDATE tbl_promo SET payment_status=1 WHERE id_promo='".$_GET['paid']."'";
        if($conn->query($query)){
            echo "<script>alert('Data sucessfully saved !');</script>";
            echo "<script>location='promo.php?status=1';</script>";
        }else{
            echo "<script>alert('Gagal mengubah data.');</script>";
        }
    }
?>

<div class="main-panel">
    <!-- Content -->
    <div class="content">
        <div class="container-fluid">            
        <!-- Form Add Mode -->
        <?php
            if ($mode=="delete") {
                $conn->query("DELETE FROM tbl_bundle WHERE id_bundle='$id_bundle'");
                echo "<script>alert('Data sucessfully deleted !');</script>";
                echo "<script>location='promo.php';</script>";
            }else{
        ?>
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-6">
                    <div class="card">
                        <div class="card-header card-header-rose">
                            <h4 class="card-title"><?php echo ($mode=="edit"? "Edit" : "Add");?> Bundle</h4>
                        </div>
                        <div class="card-body">
                            <form method="POST" enctype="multipart/formdata">
                                <?php 
                                    if($mode == "edit"){
                                        while($row=$result_edit->fetch_assoc()){
                                 ?>
                                    <div class="form-group">
                                        <label>Nama Bundle</label>
                                        <input class="form-control" placeholder="Masukkan Nama Bundle" type="text" name="nama" value="<?php echo $row['nama'];?>" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Harga Bundle</label>
                                        <input class="form-control" placeholder="Masukkan Harga Bundle" type="text" name="harga" value="<?php echo $row['price'];?>" required>
                                    </div>
                                    <input class="btn btn-sm btn-rose pull-right" type="submit" name="save" value="Edit">
                                    <a href="promo.php" class="btn btn-sm btn-default pull-right">Cancel</a>
                                <?php }
                                    }else{
                                ?>
                                    <div class="form-group">
                                        <label>Nama Bundle</label>
                                        <input class="form-control" placeholder="Masukkan Nama Bundle" type="text" name="nama"  required>
                                    </div>
                                    <div class="form-group">
                                        <label>Harga Bundle</label>
                                        <input class="form-control" placeholder="Masukkan Harga Bundle" type="text" name="harga" required>
                                    </div>
                                    <input class="btn btn-sm btn-rose pull-right" type="submit" name="save" value="Add">
                                <?php }?>
                            </form>
                        </div>
                    </div>
                </div>
            <?php } ?>
                <div class="col-md-8 col-lg-8 col-sm-6">
                    <div class="card">
                        <div class="card-header card-header-rose">
                            <h4 class="card-title">Bundle</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table cellspacing="0" class="table" id="dtBasicExample" width="100%">
                                    <thead class="text-primary">
                                        <tr>
                                            <td>ID Bundle</td>
                                            <td>Nama</td>
                                            <td>Harga</td>
                                            <td>Action</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php while($row=$bundle->fetch_assoc()){ ?>
                                        <tr>
                                            <td><?php echo $row['id_bundle']; ?></td>
                                            <td><?php echo $row['nama']; ?></td>
                                            <td><?php echo number_format($row['price']); ?></td>
                                            <td>
                                                <a href="?mode=edit&&bundle=<?php echo $row['id_bundle'];?>" class="btn btn-success btn-sm" rel="tooltip" data-original-title="Edit">
                                                    Edit
                                                </a>
                                                <a href="?mode=delete&&bundle=<?php echo $row['id_bundle'];?>" class="btn btn-danger btn-sm" rel="tooltip" data-original-title="Delete">
                                                    Delete
                                                </a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- Tabel Daftar Promo-->
            <div class="card">
                <div class="card-header card-header-tabs card-header-rose">
                    <div class="nav-tabs-navigation">
                        <div class="nav-tabs-wrapper">
                            <ul class="nav nav-tabs" data-tabs="tabs">
                                <li class="nav-item">
                                    <a class="nav-link <?php if($status == 1) echo 'active';?>" href="?status=1">
                                        Active
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link <?php if($status == 0) echo 'active';?>" href="?status=0">
                                        Unpaid
                                    </a>
                                </li>
                                <li class="nav-item pull-right">
                                    <a href="form_promo.php?mode=add" class="btn-secondary btn btn-sm pull-right" rel="tooltip" data-original-title="Add New Promo">
                                        <i class="material-icons">add_circle_outline</i> Add
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table cellspacing="0" class="table table-shopping" id="dtBasicExample" width="100%">
                            <thead class="text-primary">
                                <tr class="text-center">
                                    <th>ID Promo</th>
                                    <th colspan="2">Promo</th>
                                    <th>Durasi</th>                                    
                                    <th>Start</th>
                                    <th>End</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                while ($row=$promo->fetch_assoc()){ 
                                ?>
                                <tr class="text-center">
                                    <td><?php echo $row['id_promo']; ?></td>
                                    <td><img src="<?php echo $icon_loct, $row['banner']; ?>" width="150px" height="70px"/></td>
                                    <td class="text-left">
                                        <?php echo $row['mitra'];?><br/>
                                        <a href="detail_promo.php?id=<?php echo $row['id_promo'];?>" class="td-name"><?php echo $row['nama'];?></a>
                                    </td>
                                    <td><?php echo $row['tipe_bundle'];?></td>
                                    <td><?php echo $row['tgl_berlaku'];?></td>
                                    <td><?php echo $row['tgl_selesai'];?></td>
                                    <td>
                                        <?php if($status==1){ ?>
                                            <a href="form_promo.php?mode=edit&&id=<?php echo $row['id_promo'];?>" class="btn btn-success btn-sm" rel="tooltip" data-original-title="Edit">Edit</a>
                                        <?php }else{ ?>
                                            <a href="?paid=<?php echo $row['id_promo'];?>" class="btn btn-success btn-sm" rel="tooltip" data-original-title="Verifikasi">Verifikasi Pembayaran</a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--End Tabel Daftar Promo-->
        </div>
    </div>
    <!-- End Content -->
</div>
</body>
<?php
    include 'include/footer.php';
?>
</html>
