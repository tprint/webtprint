<?php

	$page = 'Delivery';// session
	session_start();
	if(!isset($_SESSION['loginadmin'])) {
		header('location:login.php');
	}else {
		$loginadmin = $_SESSION['loginadmin'];
	}
	
	include 'core/init.php';
  	include 'include/header.php';
  	include 'include/footer.php';
	include 'include/sidebar.php';
?>

<!-- Content -->
	<div class="main-panel">
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header card-header-rose">
								<h4 class="card-title">Tarif/km</h4>
							</div>
							
								<div class="card-body">
								<form method="POST" enctype="multipart/form-data">
									<div class="form-group">
										<input class="form-control" placeholder="Masukkan Tarif" type="text" name="tarif" required>
									</div>
									<div class="form-group">
										<input class="form-control" placeholder="Tanggal" type="date" name="tanggal" required>
									</div>
										<input class="btn btn-rose pull-right" type="submit" name="save" value="Save">
									</form>

									
									<?php
									if (isset(($_POST['save']))) {
										$tarif=$_POST['tarif'];
										$tanggal=$_POST['tanggal'];
										$conn->query("UPDATE tbl_delivery set tarif='$tarif', tanggal='$tanggal'WHERE id_delivery='$_GET[id]' ");
										echo "<script>alert('Data berhasil di simpan');</script>";
										echo "<script>location='delivery.php';</script>";
									}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</body>
</html>
