<?php
	$page = 'Transaksi';
    
    // session
    session_start();
    if(!isset($_SESSION['loginadmin'])) {
      header('location:login.php');
    }else {
      $loginadmin = $_SESSION['loginadmin'];
    }

	include 'core/init.php';
    include 'include/header.php';
	include 'include/sidebar.php';
    include 'include/navbar.php';
	
	//Query Transaksi
	$status=$_GET['status'];
	$query="SELECT tbl_transaksi.id_transaksi, 
            tbl_transaksi.tgl_pesan, 
            tbl_user.nama AS nama_user, 
            tbl_mitra.nama AS nama_mitra, 
            tbl_kategori.nama AS nama_kategori, 
            tbl_transaksi.harga_total
	        FROM tbl_transaksi, tbl_user, tbl_produk, tbl_kategori, tbl_mitra
	        WHERE tbl_transaksi.status='".$status."'
	        AND tbl_user.id_user = tbl_transaksi.id_user 
	        AND tbl_produk.id_produk = tbl_transaksi.id_produk 
	        AND tbl_kategori.id_kategori = tbl_produk.id_kategori
	        AND tbl_mitra.id_mitra = tbl_transaksi.id_mitra";
	$result = mysqli_query($conn, $query);
?>
	<div class="main-panel">
        <!-- Content -->
        <div class="content">
            <!-- Table View-->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header card-header-rose">
                                <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
                                <div class="nav-tabs-navigation">
                                    <div class="nav-tabs-wrapper">
                                        <ul class="nav nav-tabs" data-tabs="tabs">
                                            <li class="nav-item">
                                                <a class="nav-link <?php if($status == 0) echo 'active';?>" href="?status=0">
                                                    Belum Diproses
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link <?php if($status == 1) echo 'active';?>" href="?status=1">
                                                    Sedang Diproses
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link <?php if($status == 2) echo 'active';?>" href="?status=2">
                                                    Selesai
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class=" text-rose">
                                            <tr>
                                                <th>ID</th>
                                                <th>Tgl Order</th>
                                                <th>User</th>
                                                <th>Mitra</th>
                                                <th>Kategori</th>
                                                <th>Harga</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php while($row=$result->fetch_assoc()){ ?>
                                            <tr >
                                                <td><?php echo $row['id_transaksi']; ?></td>
                                                <td><?php echo $row['tgl_pesan']; ?></td>
                                                <td><?php echo $row['nama_user']; ?></td>
                                                <td><?php echo $row['nama_mitra']; ?></td>
                                                <td><?php echo $row['nama_kategori']; ?></td>
                                                <td><?php echo number_format($row['harga_total']); ?></td>
                                                <td>
                                                    <a href="detail_transaksi.php?id=<?php echo $row['id_transaksi']; ?>" class="btn btn-success btn-sm" rel="tooltip" data-original-title="See Detail Transaction">Detail</a>
                                                </td>
                                            </tr/>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- End Card Body-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Table View-->
        </div>
        <!-- End Content-->
	</div>
</body>
<?php
    include 'include/footer.php';
?>
</html>
