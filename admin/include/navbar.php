<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top">
    <div class="container-fluid">
        <ul class="navbar-wrapper navbar-collapse justify-content-end">
            <li class="navbar-brand">
                <a class="text-rose" href="#pablo" rel="tooltip" data-original-title="Edit Profile">
                    <div class="row">
                        <p><i class="material-icons">person</i> <?php echo $_SESSION['username'];?></p>
                    </div>
                </a>
            </li>
        </ul>
    </div>
</nav>
<!-- End Navbar -->