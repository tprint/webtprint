<?php
	$page = 'Kategori';

    // session
    session_start();
    if(!isset($_SESSION['loginadmin'])) {
      header('location:login.php');
    }else {
      $loginadmin = $_SESSION['loginadmin'];
    }
    
    $id = 0;
    $mode = "";
    $icon_loct = "upload/kategori/";

    if (isset($_GET['id'])) {
        $id = $_GET['id'];
    }
    if (isset($_GET['mode'])) {
        $mode = $_GET['mode'];
    }

	include 'core/init.php';
    include 'include/header.php';
    include 'include/sidebar.php';
    include 'include/navbar.php'
?>
    <div class="main-panel">
		<!-- Content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                <!-- Form Add Mode-->
                <?php 
                    if ($mode=='add') {
                ?>              
                    <div class="col-md-12">
						<div class="card">
							<div class="card-header card-header-rose">
								<h4 class="card-title">Add Kategori</h4>
							</div>
							<div class="card-body">
                                <form enctype="multipart/form-data" method="post">
                                    <div class="form-group">
                                        <label>Nama</label> <input class="form-control" name="nama" type="text">
                                    </div>
                                    <div class="form-group form-file-upload form-file-simple">
                                        <input type="text" class="form-control inputFileVisible" placeholder="Simple chooser...">
                                        <input type="file" name="icon" class="inputFileHidden">
                                    </div>
                                    <p align="right">
                                        <button class="btn btn-rose" name="add">Add</button>
                                        <button class="btn btn-warning" name="cancel">Cancel</button>
                                    </p>
                                </form>
							</div>								
                            <?php
                                //Query Input Data Add Kategori 
                                if (isset($_POST['add'])){
                                    $namafoto=$_FILES['icon']['name'];
                                    $lokasi=$_FILES['icon']['tmp_name'];
                                    $nama=$_POST['nama'];

                                    move_uploaded_file($lokasi, "upload/kategori/".$namafoto);

                                    $conn->query("INSERT INTO tbl_kategori (nama, icon) VALUES ('$nama', '$namafoto')");

                                    echo "<script>alert('Data succesfully saved !');</script>";
                                    echo "<script>location='kategori.php';</script>";

                                }else if (isset($_POST['cancel'])){
                                    echo "<script>location='kategori.php';</script>";
                                }

                            ?>
						</div>
					</div>
                <!--Form Edit Mode-->
                <?php
                    }elseif ($mode=="edit"){
                        $ambil = $conn->query("SELECT * FROM tbl_kategori WHERE id_kategori='$id'"); 
                        while ($pecah=$ambil->fetch_assoc()){
                ?>
                <div class="col-md-12">
						<div class="card">
							<div class="card-header card-header-rose">
								<h4 class="card-title">Edit Kategori</h4>
							</div>
							<div class="card-body">
                                <form enctype="multipart/form-data" method="post">
                                    <div class="form-group">
                                        <label>Nama</label> 
                                        <input class="form-control" name="nama" type="text" value="<?php echo $pecah['nama'];?>">
                                    </div>
                                    <div class="form-group form-file-upload form-file-simple">
                                        <input type="text" class="form-control inputFileVisible" value="<?php echo $pecah['icon'];?>" placeholder="Simple chooser...">
                                        <input class="inputFileHidden" name="icon" type="file">
                                    </div>
                                    <p align="right">
                                        <button class="btn btn-rose" name="edit">Edit</button>
                                        <button class="btn btn-warning" name="cancel">Cancel</button>
                                    </p>
                                </form>
							</div>								
                            <?php
                                //Query Input Data Edit Kategori 
                                if (isset($_POST['edit'])){
                                    $namafoto=$_FILES['icon']['name'];
                                    $lokasifoto=$_FILES['icon']['tmp_name'];
                                    $nama=$_POST['nama'];

                                    if ($namafoto != ''){
                                        move_uploaded_file($lokasifoto, "api/upload/kategori/$namafoto");
                                        
                                        $conn->query("UPDATE tbl_kategori SET nama='$nama', icon='$namafoto' WHERE id_kategori='$_GET[id]'");

                                    } else {
                                        $conn->query("UPDATE tbl_kategori SET nama='$nama' WHERE id_kategori='$_GET[id]'");
                                    }

                                    echo "<script>alert('Data succesfully saved !');</script>";
                                    echo "<script>location='kategori.php';</script>";

                                }else if (isset($_POST['cancel'])){
                                    echo "<script>location='kategori.php';</script>";
                                }

                            ?>
						</div>
					</div>
                <?php
                        } //end while
                    }elseif ($mode=="delete"){
                        $query_file = $conn->query("SELECT icon FROM tbl_kategori WHERE id_kategori='$id'");
                        while ($row=$query_file->fetch_assoc()) {
                            $file = $icon_loct . "" . $row['icon'];
                            if (!unlink($file)){
                                echo "<script>alert('Delete failed !');</script>";
                            }else{
                                $conn->query("DELETE FROM tbl_kategori WHERE id_kategori='$id'");
                                echo "<script>alert('Data sucessfully deleted !');</script>";
                                echo "<script>location='admin.php';</script>";
                            }
                        }
                    }else{ 
                ?>
                    <!-- Add Menu-->
                    <div class="container-fluid">
                        <ul class="navbar-wrapper navbar-collapse justify-content-end">
                            <li class="navbar-brand pull-right">
                            <?php if ($mode!="add" || $mode!="edit") { ?>
                                <a href="?mode=add" class="btn btn-rose btn" rel="tooltip" data-original-title="Add New Kategori"><i class="material-icons">add_circle_outline</i> Add</a>
                            <?php }?>
                            </li>
                        </ul>
                    </div>
                    <!-- End Add Menu -->
                <?php
                        $ambil = $conn->query("SELECT * FROM tbl_kategori"); 
                        while ($pecah=$ambil->fetch_assoc()){
                ?>
                <!--View List Mode-->
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card card-stats">
                            <div class="card-header card-header-icon">
                                <div class="card-icon">
                                    <img src="<?php echo $icon_loct, $pecah['icon']; ?>" width="70" height="70">
                                </div>
                                <h4 class="card-title"><?php echo $pecah['nama'];?></h4>
                            </div>
                            <div class="card-footer">
                                <div class="stats">
                                    <a href="?mode=edit&&id=<?php echo $pecah['id_kategori']; ?>" class="btn btn-info btn-sm" rel="tooltip" data-original-title="Edit Kategori">Edit</a>
                                    <a href="?mode=delete&&id=<?php echo $pecah['id_kategori']; ?>" class="btn btn-danger btn-sm" rel="tooltip" data-original-title="Delete Kategori">Delete</a>				
                                </div>
                            </div>
                        </div>
                    </div>
                <?php 
                        } //end while
                    } //end else mode
                ?>
                </div><!--End row-->
            </div>
        </div>
        <!-- End Content -->
	</div>
</body>
<?php
    include 'include/footer.php';
?>
<script>
// FileInput
$('.form-file-simple .inputFileVisible').click(function() {
      $(this).siblings('.inputFileHidden').trigger('click');
    });

    $('.form-file-simple .inputFileHidden').change(function() {
      var filename = $(this).val().replace(/C:\\fakepath\\/i, '');
      $(this).siblings('.inputFileVisible').val(filename);
    });

    $('.form-file-multiple .inputFileVisible, .form-file-multiple .input-group-btn').click(function() {
      $(this).parent().parent().find('.inputFileHidden').trigger('click');
      $(this).parent().parent().addClass('is-focused');
    });

    $('.form-file-multiple .inputFileHidden').change(function() {
      var names = '';
      for (var i = 0; i < $(this).get(0).files.length; ++i) {
        if (i < $(this).get(0).files.length - 1) {
          names += $(this).get(0).files.item(i).name + ',';
        } else {
          names += $(this).get(0).files.item(i).name;
        }
      }
      $(this).siblings('.input-group').find('.inputFileVisible').val(names);
    });

    $('.form-file-multiple .btn').on('focus', function() {
      $(this).parent().siblings().trigger('focus');
    });

    $('.form-file-multiple .btn').on('focusout', function() {
      $(this).parent().siblings().trigger('focusout');
    });
</script>
</html>
