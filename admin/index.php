<?php
	$page = 'Index';
	
	session_start();
	if(!isset($_SESSION['loginadmin'])) {
		header('location:login.php');
	}else {
		$loginadmin = $_SESSION['loginadmin'];
	}

	//include connect to database
	require_once 'core/init.php'; 
	include 'include/header.php';
	include 'include/sidebar.php';
	include 'include/navbar.php';
	
?>

<body class="">
	<div class="wrapper ">
		<div class="main-panel">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-4 col-md-6 col-sm-6">
							<a href="transaksi.php">
								<div class="card card-stats">
									<div class="card-header card-header-success card-header-icon">
										<div class="card-icon">
											<i class="material-icons">assignment</i>
										</div>
										<p class="card-category">Transaksi</p>
										<h3 class="card-title"> 
											<?php
											$query = "SELECT * FROM tbl_transaksi";
											$result = mysqli_query($conn, $query);
											$row = mysqli_fetch_array($result);
											echo mysqli_num_rows($result);
											?>
											<small>Transaksi</small>
										</h3>
									</div>
									<div class="card-footer">
										<div class="stats">
											<i class="material-icons">date_range</i> .....
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-6">
							<a href="user.php">
								<div class="card card-stats">
									<div class="card-header card-header-danger card-header-icon">
										<div class="card-icon">
											<i class="material-icons">supervisor_account</i>
										</div>
										<p class="card-category">User</p>
										<h3 class="card-title"> 
											<?php
											$query = "SELECT * FROM tbl_user";
											$result = mysqli_query($conn, $query);
											$row = mysqli_fetch_array($result);
											echo mysqli_num_rows($result);
											?>
											<small>User</small>
										</h3>
									</div>
									<div class="card-footer">
										<div class="stats">
											<i class="material-icons">local_offer</i> .....
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-6">
							<a href="mitra.php">
								<div class="card card-stats">
									<div class="card-header card-header-info card-header-icon">
										<div class="card-icon">
											<i class="material-icons">store_mall_directory</i>
										</div>
										<p class="card-category">Mitra</p>
										<h3 class="card-title">
											<?php
											$query = "SELECT * FROM tbl_mitra";
											$result = mysqli_query($conn, $query);
											$row = mysqli_fetch_array($result);
											echo mysqli_num_rows($result);
											?>
											<small>Mitra</small>
										</h3>
									</div>
									<div class="card-footer">
										<div class="stats">
											<i class="material-icons">store_mall_directory</i> ....
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-6">
							<a href="kategori.php">
								<div class="card card-stats">
									<div class="card-header card-header-warning card-header-icon">
										<div class="card-icon">
											<i class="material-icons">chrome_reader_mode</i>
										</div>
										<p class="card-category">Kategori</p>
										<h3 class="card-title"> 
											<?php
											$query = "SELECT * FROM tbl_kategori";
											$result = mysqli_query($conn, $query);
											$row = mysqli_fetch_array($result);
											echo mysqli_num_rows($result);
											?>
											<small>Kategori</small>
										</h3>
									</div>
									<div class="card-footer">
										<div class="stats">
											<i class="material-icons">chrome_reader_mode</i> .....
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-6">
							<a href="admin.php">
								<div class="card card-stats">
									<div class="card-header card-header-primary card-header-icon">
										<div class="card-icon">
											<i class="material-icons">person</i>
										</div>
										<p class="card-category">Admin</p>
										<h3 class="card-title"> 
											<?php
											$query = "SELECT * FROM tbl_admin";
											$result = mysqli_query($conn, $query);
											$row = mysqli_fetch_array($result);
											echo mysqli_num_rows($result);
											?>
											<small>Admin</small>
										</h3>
									</div>
									<div class="card-footer">
										<div class="stats">
											<i class="material-icons">person</i> .....
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-6">
							<a href="delivery.php">
								<div class="card card-stats">
									<div class="card-header card-header-rose card-header-icon">
										<div class="card-icon">
											<i class="material-icons">local_shipping</i>
										</div>
										<p class="card-category">Delivery Setting</p>
										<h3 class="card-title"> 
											<small>Rp </small>
											<?php
											$query = "SELECT * FROM tbl_delivery";
											$result = mysqli_query($conn, $query);
											$row = mysqli_fetch_array($result);
											echo $row['tarif'];
											?>
										</h3>
									</div>
									<div class="card-footer">
										<div class="stats">
											<i class="material-icons">date_range</i> .....
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
				<div class="row">
					
					<div class="col-md-12">
						<div class="card card-chart">
							<div class="card-header card-header-white">
								<canvas id="myAreaChart" width="400" height="100"></canvas>                  
							</div>
							<div class="card-body">
								<h4 class="card-title">Monthly Transactions</h4>
							</div>
						</div>
					</div>

				</div>				
			</div>
		</div>
	</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
	<?php include 'chart_render.php'; ?>
</body>
<?php
	include 'include/footer.php'
?>