<?php

	$page = 'Delivery';

	// session
	session_start();
	if(!isset($_SESSION['loginadmin'])) {
		header('location:login.php');
	}else {
		$loginadmin = $_SESSION['loginadmin'];
	}

	include 'core/init.php';
  	include 'include/header.php';
	include 'include/sidebar.php';
	include 'include/navbar.php';
?>

<!-- Content -->
	<div class="main-panel">
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header card-header-rose">
								<h4 class="card-title">Tarif/km</h4>
							</div>
							
								<div class="card-body">
									<div class="table-responsive">
										<table cellspacing="0" class="table" id="dtBasicExample" width="100%">
											<thead class=" text-rose">
												<tr>
													<th>No.</th>
													<th>Tarif</th>
													<th>Tanggal</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
											<?php $nomor=1 ?>
											<?php $result = $conn->query("SELECT * FROM tbl_delivery")?>
											<?php while ($pecah = $result->fetch_assoc()) { ?>
												<tr>
													<td><?php echo $nomor; ?></td>
													<td><?php echo number_format($pecah['tarif']); ?></td>
													<td><?php echo $pecah['tanggal']; ?></td>
													<td>
														<a href="edit_delivery.php?id=<?php echo $pecah['id_delivery']; ?>" class="btn btn-info btn-sm"  rel="tooltip" data-original-title="Edit Delivery Cost">Edit</a>
													</td>
												
												</tr>
												<?php $nomor++; ?>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</body>
<?php
    include 'include/footer.php';
?>
</html>