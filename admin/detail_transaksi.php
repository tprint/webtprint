<?php
	$page = 'Transaksi';
	$kategori_src = "upload/kategori";
	include 'core/init.php';
  include 'include/header.php';
	include 'include/sidebar.php';
	
    session_start();
    if(!isset($_SESSION['loginadmin'])) {
      header('location:login.php');
    }else {
      $loginadmin = $_SESSION['loginadmin'];
	}
	
	//Query Select Data Transaksi
	$query="SELECT tbl_transaksi.id_transaksi, 
		tbl_transaksi.id_mitra,
		tbl_transaksi.id_user,
		tbl_transaksi.id_file,
		tbl_user.nama AS nama_user, 
		tbl_mitra.nama AS nama_mitra,                                                     
		tbl_file.nama AS nama_file,
		tbl_kategori.nama AS nama_kategori, 
		tbl_transaksi.tgl_pesan,
		tbl_transaksi.tgl_delivery,
		tbl_transaksi.tgl_selesai,
		tbl_transaksi.paper_qty,
		tbl_transaksi.copy_qty,
		tbl_transaksi.keterangan,
		tbl_transaksi.status, 
		tbl_transaksi.ongkir, 
		tbl_transaksi.harga_satuan,
		tbl_transaksi.harga_total,
		tbl_kategori.icon,
		tbl_alamat_user.alamat,
		tbl_alamat_user.provinsi,
		tbl_alamat_user.kota,
		tbl_alamat_user.kecamatan
		FROM tbl_transaksi, tbl_user, tbl_mitra, tbl_kategori, tbl_produk, tbl_file, tbl_alamat_user
		WHERE tbl_transaksi.id_transaksi='$_GET[id]'
		AND tbl_user.id_user=tbl_transaksi.id_user
		AND tbl_mitra.id_mitra=tbl_transaksi.id_mitra
		AND tbl_produk.id_produk=tbl_transaksi.id_produk
		AND tbl_produk.id_kategori=tbl_kategori.id_kategori
		AND tbl_file.id_file=tbl_transaksi.id_file
		AND tbl_alamat_user.id_user=tbl_transaksi.id_user
		AND tbl_alamat_user.id_alamat=tbl_transaksi.id_maps";
	$result = $conn->query($query) or die($conn->error);
	$row = mysqli_fetch_assoc( $result );
?>
	<!-- Content -->
	<div class="main-panel">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-6">
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="card-icon col-md-2 text-rose">
									<i class='material-icons'>
										<?php echo ($row['status']=='0')? 'shopping_basket' : (($row['status']=='1')? 'local_shipping' : 'cloud_done');?>
									</i>
								</div>
								<h4 class="card-title">
									<?php echo ($row['status']=='0')? 'Sedang Diproses' : (($row['status']=='1')? 'Sedang Dikirim' : 'Selesai');?>
								</h4>
							</div>
						</div> <!--End card body-->
					</div> <!-- End card-->
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6">
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="card-icon col-md-2 text-rose">
									<i class='material-icons'>store</i>
								</div>
								<a href="mitra_detail.php?id=<?php echo $row['id_mitra'];?>"><h4 class="card-title"><?php echo $row['nama_mitra']; ?></h4></a>
							</div>
						</div> <!--End card body-->
					</div> <!-- End card-->
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6">
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="card-icon col-md-2 text-rose">
									<i class='material-icons'>person</i>
								</div>
								<h4 class="card-title"><?php echo $row['nama_user']; ?></h4>
							</div>
						</div> <!--End card body-->
					</div> <!-- End card-->
				</div>
			</div> <!-- End row-->
			<div class="row">
				<!-- Start Card Shipping Destination-->
				<div class="col-lg-8 col-md-6 col-sm-6">
					<div class="card">
						<div class="card-header card-header-rose">
							<h4 class="card-title">Detail Pemesanan</h4>
						</div>
						<div class="card-body">
							<!-- Start Table Produk-->
							<div class="table-responsive">
								<table class="table table-shopping">
									<thead class="text-danger text-center">
										<tr>
											<th colspan="2">Produk</th>
											<th>Halaman</th>
											<th>Harga Satuan</th>
											<th>Copy</th>
											<th>Subtotal</th>
										</tr>
									</thead>
									<tbody>
											<tr class="text-center">
												<td><img src="<?php echo $kategori_src .'/'. $row['icon'];?>" width="50px" height="50px"/></td>
												<td class="text-left">
													<?php echo $row['nama_kategori']; ?><br/>
													<a href="#" class="td-name"><?php echo $row['nama_file']; ?></a>
												</td>
												<td><?php echo $row['paper_qty']; ?></td>
												<td><?php echo number_format($row['harga_satuan']); ?></td>
												<td><?php echo $row['copy_qty']; ?></td>
												<td><?php echo number_format($row['harga_total']); ?></td>
											</tr>
									</tbody>
								</table>
							</div> <!--End Table Produk-->
							<hr/>
							<!-- Start Table Price Harga-->
							<div class="col-md-8 pull-right table-responsive">
								<table class="table table-shopping">
									<tbody class="text-right">
										<tr>
											<td>Total Pesanan : </td>
											<td>Rp. <?php echo number_format($row['harga_total']);?></td>
										</tr>
										<tr>
											<td>Ongkos Kirim : </td>
											<td>Rp. <?php echo number_format($row['ongkir']);?></td>
										</tr>
										<tr>
											<td>Total Penjualan : </td>
											<td class="td-name">Rp. <?php echo number_format($row['harga_total'] + $row['ongkir']);?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div> <!-- End Card Shipping Destination-->
				<!-- Start Card Order Detail-->
				<div class="col-lg-4 col-md-6 col-sm-6">
					<div class="card">
						<div class="card-header card-header-rose">
							<h4 class="card-title">Alamat Pengiriman</h4>
						</div>
						<div class="card-body">
							<h4 class="card-title"><?php echo $row['alamat']; ?></h6>
							<p class="card-text"><?php  echo $row['kecamatan'] . ', ' . $row['kota'] . ', ' . $row['provinsi'];?></p>
						</div>
					</div>
				</div><!-- End Card Order Detail-->
			</div>
		</div> <!-- End container fluid-->
	</div>
</body>
<?php
    include 'include/footer.php';
?>
</html>