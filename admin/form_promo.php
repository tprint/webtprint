<?php
    $page = 'Promo';
    
    // session
    session_start();
    if(!isset($_SESSION['loginadmin'])) {
      header('location:login.php');
    }else {
      $loginadmin = $_SESSION['loginadmin'];
    }

	include 'core/init.php';
    include 'include/header.php';
    include 'include/sidebar.php';
    include 'include/navbar.php';
    include 'include/footer.php';
    include_once 'api/config/database.php';
    include_once 'api/objects/id_generator.php';

    // instantiate database
    $database = new Database();
    $db = $database->getConnection();

    //initialize object
    $id_generator = new IDGenerator($db);
    
    $id = 0;
    $price = 0;
    $tax = 0;
    $total = 0;
    $mode = "";

    if (isset($_GET['id'])) {
        $id = $_GET['id'];
    }
    if (isset($_GET['mode'])) {
        $mode = $_GET['mode'];
    }

    if(isset($_POST['tambah'])){
      $id_promo = $id_generator->promotion();
      $id_mitra = htmlspecialchars($_POST['mitra']);
      $promo = htmlspecialchars($_POST['promo']);
      $detail = htmlspecialchars($_POST['detail']);
      $price = htmlspecialchars($_POST['price']);            
      $tax = $price * 10 / 100;
      $total = $price +  $tax;
      $bundle = '';
      $qBundle = $conn->query("SELECT nama FROM tbl_bundle WHERE price='$price' LIMIT 0,1");
      while($row=$qBundle->fetch_assoc()){
        $bundle = $row['nama'];
      }
      $durasi = substr($bundle, 0, 1)*30;
      $start_date = htmlspecialchars($_POST['date_promo']);
      $end_date = date('Y-m-d', strtotime($start_date . ' + ' . $durasi . ' days'));
      
      $banner=$_FILES['banner']['name'];
      $lokasi=$_FILES['banner']['tmp_name'];
      move_uploaded_file($lokasi, "upload/promosi/".$banner);
      
      $query = "INSERT INTO tbl_promo SET id_promo='$id_promo', 
                  id_mitra='$id_mitra',
                  nama='$promo',
                  banner='$banner',
                  detail='$detail',
                  tgl_berlaku='$start_date',
                  tgl_selesai='$end_date',
                  tipe_bundle='$bundle',
                  price='$price',
                  tax='$tax',
                  total='$total'";
      if($conn->query($query)){
          echo "<script>alert('Data sucessfully saved !');</script>";
          echo "<script>location='promo.php';</script>";
      }else{
          echo "<script>alert('Gagal menambahkan data.');</script>";
          echo "$id_promo, $id_mitra, $bundle, $banner, $price, $tax, $total, $start_date, $end_date, $promo, $detail";
      }
    }else if($_POST['simpan']){
      $promo = htmlspecialchars($_POST['promo']);
      $detail = htmlspecialchars($_POST['detail']);
      $price = htmlspecialchars($_POST['price']);            
      $bundle = '';
      $qBundle = $conn->query("SELECT nama FROM tbl_bundle WHERE price='$price' LIMIT 0,1");
      while($row=$qBundle->fetch_assoc()){
        $bundle = $row['nama'];
      }
      $durasi = substr($bundle, 0, 1)*30;
      $start_date = htmlspecialchars($_POST['date_promo']);
      $end_date = date('Y-m-d', strtotime($start_date . ' + ' . $durasi . ' days'));
      
      $banner=$_FILES['banner']['name'];
      $lokasi=$_FILES['banner']['tmp_name'];
      move_uploaded_file($lokasi, "upload/promosi/".$banner);
      $query = "UPDATE tbl_promo SET 
                  nama='$promo',
                  banner='$banner',
                  detail='$detail',
                  tgl_berlaku='$start_date',
                  tgl_selesai='$end_date'
                WHERE id_promo='".$_GET['id']."'";
      if($conn->query($query)){
          echo "<script>alert('Data sucessfully saved !');</script>";
          echo "<script>location='promo.php';</script>";
      }else{
          echo "<script>alert('Gagal mengubah data.');</script>";
          //echo "$bundle, $banner, $start_date, $end_date, $promo, $detail";
      }
    }
?>

    <div class="main-panel">
        <!-- Content -->
        <div class="content">
          <div class="container-fluid">            
            <form method="POST" enctype="multipart/form-data">
              <?php if($mode=='add'){ ?>
                <div class="row">
                    <div class="col-md-6 col-lg-6 col-sm-6">
                        <div class="card">
                            <div class="card-header card-header-rose">
                                <h4 class="card-title">Detail Promo</h4>
                            </div>
                            <div class="card-body">
                                    <div class="form-group dropdown bootstrap-select dropup">
                                        <label class="label-control">Mitra</label>
                                        <select class="form-control" name="mitra" required>
                                            <option value='' disabled selected>Daftar Mitra</option>
                                            <?php 
                                                $query_mitra = "SELECT * FROM tbl_mitra";
                                                $result = $conn->query($query_mitra);
                                                while($mitra = $result->fetch_assoc()){
                                            ?>
                                                <option name="mitra" value="<?php echo $mitra['id_mitra'];?>"><?php echo $mitra['nama']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="label-control">Nama</label>
                                        <input class="form-control" name="promo" type="text" placeholder="Nama Progam atau Promo" required/>
                                    </div>
                                    <div class="form-group">
                                        <label class="label-control">Deskripsi</label>
                                        <textarea class="form-control" name="detail" rows="3" placeholder="Detail atau Deskripsi Promo" required></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="label-control">Tanggal Tayang</label>
                                        <input type="date" class="form-control" name="date_promo"/>
                                    </div>
                                    <div class="form-group form-file-upload form-file-simple">
                                        <input type="text" class="form-control inputFileVisible" placeholder="Simple chooser...">
                                        <input type="file" name="banner" class="inputFileHidden" required>
                                    </div>
                                    <input class="btn btn-rose pull-right" type="submit" name="tambah" value="Add">
                            </div><!--End Card Body-->
                        </div><!-- End Card-->            
                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-6">
                        <div class="card">
                            <div class="card-header card-header-rose">
                                <h4 class="card-title">Detail Pembayaran</h4>
                            </div>
                            <div class="card-body">
                                <form method="POST" enctype="multipart/form-data">
                                    <div class="form-group dropdown bootstrap-select dropup">
                                        <label class="label-control">Bundle</label>
                                        <!-- <input type="text" name="bundle" class="form-control hidden" id="bundleHidden"/> -->
                                        <select class="form-control" name="price" id="mybundle" required>
                                            <option value='' disabled selected>Pilih Paket Bundle</option>
                                            <?php 
                                                $query_bundle = "SELECT * FROM tbl_bundle";
                                                $result = $conn->query($query_bundle);
                                                while($bundle = $result->fetch_assoc()){
                                            ?>
                                                <option name="price" id="myPrice" value="<?php echo $bundle['price'];?>">
                                                    <?php echo $bundle['nama']; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <!-- Start Table Produk-->
                                    <div class="table-responsive">
                                        <table class="table table-shopping">
                                            <tbody class="text-right">
                                                <tr>
                                                    <td>Harga : </td>
                                                    <td id="tblPrice" >Rp. <?php echo number_format($price);?></td>
                                                </tr>
                                                <tr>
                                                    <td>Pajak 10% : </td>
                                                    <td id="tblTax" >Rp. <?php echo number_format($tax);?></td>
                                                </tr>
                                                <tr>
                                                    <td>Total : </td>
                                                    <td id="tblTotal" class="td-name">Rp. <?php echo number_format($total);?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div> <!--End Table Produk-->
                                </form>
                            </div><!--End Card Body-->
                        </div><!-- End Card-->            
                    </div> 
                </div><!-- End Row-->                   
                <?php }else {
                    $id_promo = $_GET['id'];
                    $qPromo = $conn->query("SELECT * FROM tbl_promo WHERE id_promo='$id_promo'");
                    while($promo = $qPromo->fetch_assoc()){
                  ?>
                <!-- EDIT MODE-->
                <div class="row">
                    <div class="col-md-6 col-lg-6 col-sm-6">
                        <div class="card">
                            <div class="card-header card-header-rose">
                                <h4 class="card-title">Detail Promo</h4>
                            </div>
                            <div class="card-body">
                                    <div class="form-group dropdown bootstrap-select dropup">
                                        <label class="label-control">Mitra</label>
                                        <select class="form-control" name="mitra" required disabled>
                                            <?php 
                                                $query_mitra = "SELECT * FROM tbl_mitra WHERE id_mitra='".$promo['id_mitra']."'";
                                                $result = $conn->query($query_mitra);
                                                while($mitra = $result->fetch_assoc()){
                                            ?>
                                                <option name="mitra" value="<?php echo $mitra['id_mitra'];?>"><?php echo $mitra['nama']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="label-control">Nama</label>
                                        <input class="form-control" name="promo" type="text" value="<?php echo $promo['nama'];?>" placeholder="Nama Progam atau Promo" required/>
                                    </div>
                                    <div class="form-group">
                                        <label class="label-control">Deskripsi</label>
                                        <textarea class="form-control" name="detail" rows="3" placeholder="Detail atau Deskripsi Promo" required><?php echo $promo['detail'];?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="label-control">Tanggal Tayang</label>
                                        <input type="text" class="form-control" name="date_promo" value="<?php echo $promo['tgl_berlaku'];?>" placeholder="YYYY-MM-DD" rel="tooltip" data-original-title="YYYY-MM-DD"/>
                                    </div>
                                    <div class="form-group form-file-upload form-file-simple">
                                        <input type="text" value="<?php echo $promo['banner'];?>" class="form-control inputFileVisible" placeholder="Simple chooser...">
                                        <input type="file" name="banner" class="inputFileHidden" required>
                                    </div>
                                    <input class="btn btn-rose pull-right" type="submit" name="simpan" value="Simpan">
                            </div><!--End Card Body-->
                        </div><!-- End Card-->            
                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-6">
                        <div class="card">
                            <div class="card-header card-header-rose">
                                <h4 class="card-title">Detail Pembayaran</h4>
                            </div>
                            <div class="card-body">
                                <form method="POST" enctype="multipart/form-data">
                                    <div class="form-group dropdown bootstrap-select dropup">
                                        <label class="label-control">Bundle</label>
                                        <!-- <input type="text" name="bundle" class="form-control hidden" id="bundleHidden"/> -->
                                        <select class="form-control" name="price" id="mybundle" required disabled>
                                            <?php 
                                                $query_bundle = "SELECT * FROM tbl_bundle WHERE price='" . $promo['price']."'";
                                                $result = $conn->query($query_bundle);
                                                while($bundle = $result->fetch_assoc()){
                                            ?>
                                                <option name="price" id="myPrice" value="<?php echo $promo['price'];?>">
                                                    <?php echo $bundle['nama']; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <!-- Start Table Produk-->
                                    <div class="table-responsive">
                                        <table class="table table-shopping">
                                            <tbody class="text-right">
                                                <tr>
                                                    <td>Harga : </td>
                                                    <td id="tblPrice" >Rp. <?php echo number_format($promo['price']);?></td>
                                                </tr>
                                                <tr>
                                                    <td>Pajak 10% : </td>
                                                    <td id="tblTax" >Rp. <?php echo number_format($promo['tax']);?></td>
                                                </tr>
                                                <tr>
                                                    <td>Total : </td>
                                                    <td id="tblTotal" class="td-name">Rp. <?php echo number_format($promo['total']);?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div> <!--End Table Produk-->
                                </form>
                            </div><!--End Card Body-->
                        </div><!-- End Card-->            
                    </div> 
                </div><!-- End Row-->                   
                <?php } }?>
              </form>
            </div>
        </div><!-- End Content -->
	</div>
</body>
<script>
$(document).ready(function() {
      if ($('.card-header.card-chart').length != 0) {
        md.initDashboardPageCharts();
      }

      if ($('#websiteViewsChart').length != 0) {
        md.initDocumentationCharts();
      }

      if ($('.datetimepicker').length != 0) {
        md.initFormExtendedDatetimepickers();
      }
      if ($('#fullCalendar').length != 0) {
        md.initFullCalendar();
      }

      if ($('.slider').length != 0) {
        md.initSliders();
      }

      //  Activate the tooltips
      $('[data-toggle="tooltip"]').tooltip();

      // Activate Popovers
      $('[data-toggle="popover"]').popover();

      // Vector map
      if ($('#worldMap').length != 0) {
        md.initVectorMap();
      }

      setFormValidation('#RegisterValidation');

      function setFormValidation(id) {
        $(id).validate({
          highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
            $(element).closest('.form-check').removeClass('has-success').addClass('has-danger');
          },
          success: function(element) {
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
            $(element).closest('.form-check').removeClass('has-danger').addClass('has-success');
          },
          errorPlacement: function(error, element) {
            $(element).closest('.form-group').append(error);
          },
        });
      }
    });

    // FileInput
    $('.form-file-simple .inputFileVisible').click(function() {
      $(this).siblings('.inputFileHidden').trigger('click');
    });

    $('.form-file-simple .inputFileHidden').change(function() {
      var filename = $(this).val().replace(/C:\\fakepath\\/i, '');
      $(this).siblings('.inputFileVisible').val(filename);
    });

    $('.form-file-multiple .inputFileVisible, .form-file-multiple .input-group-btn').click(function() {
      $(this).parent().parent().find('.inputFileHidden').trigger('click');
      $(this).parent().parent().addClass('is-focused');
    });

    $('.form-file-multiple .inputFileHidden').change(function() {
      var names = '';
      for (var i = 0; i < $(this).get(0).files.length; ++i) {
        if (i < $(this).get(0).files.length - 1) {
          names += $(this).get(0).files.item(i).name + ',';
        } else {
          names += $(this).get(0).files.item(i).name;
        }
      }
      $(this).siblings('.input-group').find('.inputFileVisible').val(names);
    });

    $('.form-file-multiple .btn').on('focus', function() {
      $(this).parent().siblings().trigger('focus');
    });

    $('.form-file-multiple .btn').on('focusout', function() {
      $(this).parent().siblings().trigger('focusout');
    });
    // $(document).ready(function(){
    //   var date_input=$('input[name="date_promo"]'); //our date input has the name "date"
    //   var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
    //   var options={
    //     format: 'mm/dd/yyyy',
    //     container: container,
    //     todayHighlight: true,
    //     autoclose: true,
    //   };
    //   date_input.datepicker(options);
    // })
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#mybundle').change(function() {
            var bundle = $(this).val();
            var price = $(this).val();
            var tax =  price * 10 / 100;
            document.getElementById("tblPrice").innerHTML = price;
            document.getElementById("tblTax").innerHTML = tax;
            document.getElementById("tblTotal").innerHTML = parseInt(price) + parseInt(tax);
            // var idbundle = $(this).innerHTML;
            // $(this).siblings('#bundleHidden').val(idbundle);
        })
    });
</script>
</html>
