<?php
$page = 'Mitra';

include 'core/init.php';
include 'include/header.php';
include 'include/footer.php';
include 'include/sidebar.php';

$pict_loct = "http://avatar.tprint.web.id/";
	// session
session_start();
if(!isset($_SESSION['loginadmin'])) {
	header('location:login.php');
}else {
	$loginadmin = $_SESSION['loginadmin'];
}

?>

<!-- Content -->
<div class="main-panel">
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<?php
				//Query to get id 
				$ambil=$conn->query("SELECT * FROM tbl_mitra,tbl_alamat WHERE tbl_mitra.id_mitra='$_GET[id]' AND tbl_alamat.id_mitra='$_GET[id]'");
				$pecah= $ambil->fetch_assoc();
				?>
				<div class="col-md-6">
					<div class="card">
						<div class="card-header card-header-rose">
							<center><h3 class="card-title">Profil Mitra</h3></center>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table">
									<tbody>
										<tr>
											<td><b>Nama    : <?php echo $pecah['nama']; ?></b></td>
										</tr>
										<tr>
											<td><b>Alamat  : <?php echo $pecah['alamat'];?></b></td>
										</tr>
										<tr>
											<td><b>Telepon : <?php echo $pecah['telp'];?></b></td>
										</tr>
										<tr>
											<td><b>Email   : <?php echo $pecah['email'];?></b></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="card">
						<div class="card-header card-header-rose">
							<center><h4 class="card-title">Foto Mitra</h4></center>
						</div>
						<div class="card-body">
							<center><img src="<?php echo $pict_loct, $pecah['logo']; ?>" width="300" height="300"></center>
						</div>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header card-header-rose">
					<center><h4 class="card-title">Produk</h4></center>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table">
							<thead class=" text-rose">
								<?php
								//Query to get id 
									$ambil=$conn->query("SELECT * FROM tbl_produk, tbl_kategori
									WHERE id_mitra='$_GET[id]' AND tbl_produk.id_kategori = tbl_kategori.id_kategori");
								?>
								<tr>
									<th>ID Produk</th>
									<th>Kategori</th>
									<th>Ukuran</th>
									<th>Bahan</th>
									<th>Harga Hitam Putih</th>
									<th>Harga Berwarna</th>
								</tr>
							</thead>
							<tbody>
								<?php while ($pecah= $ambil->fetch_assoc()){?>
									<tr>
										<td>&nbsp&nbsp&nbsp&nbsp&nbsp<?php echo $pecah['id_produk']; ?></td>
										<td><?php echo $pecah['nama'];?></td>
										<td><?php echo $pecah['ukuran']; ?></td>
										<td><?php echo $pecah['bahan']; ?></td>
										<td>Rp <?php echo number_format($pecah['price_b'],0,".","."); ?></td>
										<td>Rp <?php echo number_format($pecah['price_c'],0,".","."); ?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>