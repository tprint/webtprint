<?php
    include '../../core/init.php';

//$key is our base64 encoded 256bit key that we created earlier. You will probably store and define this key in a config file.
$key = 'VGVrYXNoaSBjaGkgbmktc2Fu';

function my_encrypt($data, $key) {
    // Remove the base64 encoding from our key
    $encryption_key = base64_decode($key);
    // Generate an initialization vector
    $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
    // Encrypt the data using AES 256 encryption in CBC mode using our encryption key and initialization vector.
    $encrypted = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $iv);
    // The $iv is just as important as the key for decrypting, so save it with our encrypted data using a unique separator (::)
    return base64_encode($encrypted . '::' . $iv);
}

function my_decrypt($data, $key) {
    // Remove the base64 encoding from our key
    $encryption_key = base64_decode($key);
    // To decrypt, split the encrypted data from our IV - our unique separator used was "::"
    list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);
    return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
}
    $respons = array();

    if (isset($_REQUEST['id']) && isset($_REQUEST['file'])) {
        $id_user = my_decrypt($_REQUEST['id'], $key);
        $file = my_decrypt($_REQUEST['file'], $key);            
        $filepath = '/' . $id_user . '/' . $file;

        // cek if file exist
        if (file_exists($filepath)) {
            echo '<p>'.$filepath.'</p>';
            echo '<p>'.$_REQUEST['file'].'</p>';
            echo '<p>'.$_REQUEST['id'].'</p>';
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($filepath).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            flush(); // Flush system output buffer
            readfile($filepath);
            exit;
            header('Location: http://mitra.tprint.web.id/transaksi.php');
        } else {
            // set response code - 404 Not found
            http_response_code(404);
        
            // tell the user no products found
            $respons['message'] = "File not found. " . $filepath;
            echo json_encode($respons);
        }
        
    }
?>