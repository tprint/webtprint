<?php
	//Import File Koneksi Database
	include('koneksi.php');
	
	//Membuat SQL Query
	$sql = "SELECT * FROM tb_pegawai";
	
	//Mendapatkan Hasil
	$r = mysqli_query($con,$sql);
	
	//Membuat Array Kosong 
	$result = array();
	
	while($row = $r->fetch_assoc()){
		
		//Memasukkan Nama dan ID kedalam Array Kosong yang telah dibuat 
		array_push($result,array(
			"id"=>$row['id'],
			"name"=>$row['nama'],
			"umur"=>$row['umur'],
			"position"=>$row['posisi'],
			"salary"=>$row['gaji']
			));
	}
	
	//Menampilkan Array dalam Format JSON
	echo json_encode(array('pegawai'=>$result));
?>